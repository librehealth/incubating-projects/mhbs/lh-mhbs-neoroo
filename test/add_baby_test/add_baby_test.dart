import 'dart:typed_data';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:neoroo/bloc/add_baby_bloc/add_baby_bloc.dart';
import 'package:neoroo/bloc/add_baby_bloc/add_baby_events.dart';
import 'package:neoroo/bloc/add_baby_bloc/add_baby_states.dart';
import 'package:neoroo/models/infant_model.dart';
import 'package:neoroo/models/infant_mother.dart';
import 'package:neoroo/models/profile.dart';
import 'package:neoroo/repository/add_update_baby_repository.dart';
import 'package:neoroo/repository/eceb_to_neoroo_repository.dart';
import 'package:neoroo/repository/hive_storage_repository.dart';
import 'package:image_picker/image_picker.dart';
import 'add_baby_test.mocks.dart' as mocks;

@GenerateMocks(
    [AddUpdateBabyRepository, HiveStorageRepository, ECEBtoNeoRooRepository])
void main() {
  late mocks.MockAddUpdateBabyRepository addUpdateBabyRepository;
  late mocks.MockHiveStorageRepository hiveStorageRepository;
  late mocks.MockECEBtoNeoRooRepository eceBtoNeoRooRepository;

  setUp(() {

    addUpdateBabyRepository = mocks.MockAddUpdateBabyRepository();
    hiveStorageRepository = mocks.MockHiveStorageRepository();
    eceBtoNeoRooRepository = mocks.MockECEBtoNeoRooRepository();
        when(addUpdateBabyRepository.addBaby(
            any, any, any, any, any, any, any, any, any, any, any, any, any, any, any, any, any, any, any, any, any, any, any, any,))
        .thenAnswer((_) async => Left(true));
  });

  group('AddBabyBloc', () {
    // Test for getMother method
    final bytes = Uint8List.fromList([0x50, 0x4E, 0x47]);
    final xfile = XFile.fromData(bytes);

    final mockProfile = Profile(
      'John Doe',
      'johndoe',
      'password',
      null,
      'user123',
      'user',
    );

    final mockInfant = Infant(
      infantId: 'infant123',
      moterName: 'Mother Name',
      motherUsername: 'mother123',
      dateOfBirth: '2023-08-25',
      timeOfBirth: '12:00 PM',
      birthWeight: '3.5 kg',
      bodyLength: '50 cm',
      headCircumference: '35 cm',
      birthNotes: 'Normal delivery',
      resuscitation: 'No',
      neoTemperature: '37.2°C',
      neoHeartRate: '120 bpm',
      neoRespiratoryRate: '30 bpm',
      neoOxygenSaturation: '98%',
      neoSTS: '5 min',
      neoNSTS: '10 min',
      infantTrackedInstanceID: 'instance123',
      cribNumber: 'Crib 1',
      wardNumber: 'Ward A',
      presentWeight: '3.3 kg',
      neoDeviceID: 'device123',
      avatarID: 'avatar123',
      goals: 'Healthy development',
    );
    final mockInfantList = [mockInfant];

    final mockMotherList = [
      Mother("Jane Don", "wqtjvskkc", "jane"),
      Mother("Emma Potter", "qwertyip7", "ema"),
    ];
    blocTest<AddBabyBloc, AddBabyStates>(
      'emits SearchMotherInitialState and SearchMotherState when getMother is called',
      build: () => AddBabyBloc(hiveStorageRepository, addUpdateBabyRepository,
          eceBtoNeoRooRepository),
      act: (bloc) {
        when(addUpdateBabyRepository.getMother()).thenAnswer(
          (_) async => Left(mockMotherList),
        );
        bloc.add(GetMotherEvent());
      },
      expect: () => [
        SearchMotherInitialState(),
        SearchMotherState(mockMotherList),
      ],
    );

    // Test for searchMotherInList method
    blocTest<AddBabyBloc, AddBabyStates>(
      'emits SearchMotherState when searchMotherInList is called',
      build: () => AddBabyBloc(hiveStorageRepository, addUpdateBabyRepository,
          eceBtoNeoRooRepository),
      act: (bloc) {
        final event = SearchInMotherList("Emma", mockMotherList);
        bloc.add(event);
      },
      expect: () => [
        SearchMotherState(mockMotherList),
      ],
    );

    // Test for searchInfantFromECEB method
    blocTest<AddBabyBloc, AddBabyStates>(
      'emits FetchBabyFromECEBSucess when searchInfantFromECEB is called',
      build: () => AddBabyBloc(hiveStorageRepository, addUpdateBabyRepository,
          eceBtoNeoRooRepository),
      act: (bloc) {
        final event = SearchInfantFromECEBList(
            infantData: "baby", ecebInfantSearchList: mockInfantList);
        bloc.add(event);
      },
      expect: () => [
        FetchBabyFromECEBSucess(ecebInfantsOnServer: mockInfantList),
      ],
    );

    // Test for addBaby method with valid data
    blocTest<AddBabyBloc, AddBabyStates>(
      'emits LoadingAddBaby and AddBabySuccess when addBaby is called with valid data',
      build: () => AddBabyBloc(hiveStorageRepository, addUpdateBabyRepository,
          eceBtoNeoRooRepository),
      act: (bloc) async {
        final event = AddBabyEvent(
            "20-12-2023",
            "Note",
            "5 pm",
            "450 g",
            "122 cm",
            "22",
            "1234",
            "90 cm",
            xfile,
            "No",
            "12",
            "450 gm",
            "Emma Potter",
            "12344",
            "5 hrs",
            "6 hra",
            "70",
            "hearrate",
            "respiration",
            "blood",
            "qwerty1234");
        when(hiveStorageRepository.getUserProfile())
            .thenAnswer((_) async => mockProfile);
        when(hiveStorageRepository.getOrganisationURL())
            .thenAnswer((_) async => "https://bmgfdev.soic.iupui.edu");
        when(hiveStorageRepository.getSelectedOrganisation())
            .thenAnswer((_) async => "yrqWrtrun");
        when(addUpdateBabyRepository.addBaby(
                "20-12-2023",
                "Note",
                "5 pm",
                "450 g",
                "122 cm",
                "22",
                "1234",
                "90 cm",
                "",
                "No",
                "12",
                "450 gm",
                "Emma Potter",
                "12344",
                "5 hrs",
                "6 hra",
                "70",
                "hearrate",
                "respiration",
                xfile,
                "qwerty1234",
                "username",
                "password",
                "serverurl"))
            .thenAnswer((_) async => Left(true)); 
        bloc.add(event);
      },
      expect: () => [
        LoadingAddBaby(),
        AddBabySuccess(),
      ],
    );


    // Test for getInfantsFromEceb method
    blocTest<AddBabyBloc, AddBabyStates>(
      'emits FetchInfantFromECEBInitialState and FetchBabyFromECEBSucess when getInfantsFromEceb is called',
      build: () => AddBabyBloc(hiveStorageRepository, addUpdateBabyRepository,
          eceBtoNeoRooRepository),
      act: (bloc) {
        final event = GetInfantsFromEceb();
        when(eceBtoNeoRooRepository.fetchBabyFromECEB()).thenAnswer(
          (_) async => Left(mockInfantList),
        );
        bloc.add(event);
      },
      expect: () => [
        FetchInfantFromECEBInitialState(),
        FetchBabyFromECEBSucess(ecebInfantsOnServer: mockInfantList),
      ],
    );

    // Test for ecebInfantSelected method
    blocTest<AddBabyBloc, AddBabyStates>(
      'emits EcebInfantSelectedState when ecebInfantSelected is called',
      build: () => AddBabyBloc(hiveStorageRepository, addUpdateBabyRepository,
          eceBtoNeoRooRepository),
      act: (bloc) {
        final event = EcebInfantSelected(infant: mockInfant);
        bloc.add(event);
      },
      expect: () => [
        EcebInfantSelectedState(infant: mockInfant),
      ],
    );
  });
}
