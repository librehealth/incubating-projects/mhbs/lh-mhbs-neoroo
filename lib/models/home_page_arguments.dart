class HomePageArguments{
  const HomePageArguments({ required this.userName, required this.userPictureURL, required this.alerts});
  final String userName;
  final String userPictureURL;
  // TODO: replace String with HighPriorityAlert class name (model)
  final List<Map<String, dynamic>> alerts; //changed to mathch home_page.dart
}