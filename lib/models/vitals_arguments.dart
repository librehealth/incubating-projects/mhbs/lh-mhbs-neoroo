import 'package:neoroo/utils/enums.dart';

class VitalsArguments {
  const VitalsArguments({
    required this.temperatureSeverity,
    required this.heartRateSeverity,
    required this.respirationSeverity,
    required this.oxygenSaturationSeverity,

});
  final VitalSeverity temperatureSeverity;
  final VitalSeverity heartRateSeverity;
  final VitalSeverity respirationSeverity;
  final VitalSeverity oxygenSaturationSeverity;
}