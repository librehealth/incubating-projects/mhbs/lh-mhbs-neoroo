import 'package:flutter/material.dart';
import 'package:neoroo/screens/add_edit_baby_details/components/success_dialog.dart';
import 'package:neoroo/screens/authentication_screens/components/auth_input_text_field.dart';
import 'package:neoroo/screens/landing_screen/components/main_button.dart';
import 'package:neoroo/screens/notifications_screen/notifications_screen.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:neoroo/routing/routing.dart' as router;
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';
class AddEditBabyDetails extends StatefulWidget {
  const AddEditBabyDetails({Key? key}) : super(key: key);

  @override
  State<AddEditBabyDetails> createState() => _AddEditBabyDetailsState();
}

class _AddEditBabyDetailsState extends State<AddEditBabyDetails> {
  TextEditingController? _motherNameController;
  TextEditingController? _birthDateController;
  TextEditingController? _birthTimeController;
  TextEditingController? _birthWeightController;
  TextEditingController? _bodyLengthController;
  TextEditingController? _headCircumferenceController;
  TextEditingController? _RequireBirthResucitationController;
  TextEditingController? _BirthNotesController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _motherNameController = TextEditingController();
    _motherNameController!.value = TextEditingValue(text: "Mother's Name");
    _birthDateController = TextEditingController();
    _birthDateController!.value = TextEditingValue(text: "12/07/2001");
    _birthTimeController = TextEditingController();
    _birthTimeController!.value = TextEditingValue(text: "12:00");
    _birthWeightController = TextEditingController();
    _birthWeightController!.value = TextEditingValue(text: "1300");
    _bodyLengthController = TextEditingController();
    _bodyLengthController!.value = TextEditingValue(text: "50");
    _headCircumferenceController = TextEditingController();
    _headCircumferenceController!.value = TextEditingValue(text: "30");
    _RequireBirthResucitationController = TextEditingController();
    _RequireBirthResucitationController!.value = TextEditingValue(text: "No");
    _BirthNotesController = TextEditingController();
    _BirthNotesController!.value = TextEditingValue(text: "Head to toe newborn examination on 07/21/2020; no major congenital anomalies detected. Maternal anxiety; refer to peer mother mentor.");



  }
  @override
  Widget build(BuildContext context) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    final double deviceWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: ScreenHeader(
        isUserIconVisible: false,
        title: "Baby’s Birth Details",
        subtitle: "ID: 123456789",
        hasBackButton: true,
        actionIconImages: [
        "assets/images/notification.png",
        ],
        actionIconOnPressFunctions: [() {
          pushScreen(context, screen: NotificationsScreen(), settings: RouteSettings(name: router.notificationsPage));
        },
    ],
    backButtonOnPressed:(){
      Navigator.pop(context);
    }),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            children: [
              AuthInputTextField(placeholder: "Mother's Name",
                  inputType: InputType.username,
                  controller: _motherNameController!,
                  label: "Mother's Name",
                  isRequired: false,
                  errorText: null,
                  isAuth: false,
              ),
              SizedBox(height: convertXdHeightToScreen(xdHeight: 14, deviceHeight: deviceHeight, xdScreenHeight: 680),),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: AuthInputTextField(placeholder: "Birth Date",
                      inputType: InputType.datetime,
                      controller: _birthDateController!,
                      label: "Birth Date",
                      isRequired: false,
                      errorText: null,
                      isAuth: false,
                    ),
                  ),
                  SizedBox(width: convertXdWidthToScreen(xdWidth: 16, deviceWidth: deviceWidth, xdScreenWidth: 360),),
                  Expanded(
                    child: AuthInputTextField(placeholder: "Birth Time",
                      inputType: InputType.datetime,
                      controller: _birthTimeController!,
                      label: "Birth Time",
                      isRequired: false,
                      errorText: null,
                      isAuth: false,
                      suffix: Container(
                        color: primary.withOpacity(0.1),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("AM",style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),),
                        )
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: convertXdHeightToScreen(xdHeight: 14, deviceHeight: deviceHeight, xdScreenHeight: 680),),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: AuthInputTextField(placeholder: "Birth Wt.",
                      inputType: InputType.number,
                      controller: _birthWeightController!,
                      label: "Birth Wt.",
                      isRequired: false,
                      errorText: null,
                      isAuth: false,
                      suffix: Container(
                          color: primary.withOpacity(0.1),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("grams",style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),),
                          )
                      ),
                    ),
                  ),
                  SizedBox(width: convertXdWidthToScreen(xdWidth: 16, deviceWidth: deviceWidth, xdScreenWidth: 360),),
                  Expanded(
                    child: AuthInputTextField(placeholder: "Body Length",
                      inputType: InputType.number,
                      controller: _bodyLengthController!,
                      label: "Body Length",
                      isRequired: false,
                      errorText: null,
                      isAuth: false,
                      suffix: Container(
                          color: primary.withOpacity(0.1),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("cms",style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),),
                          )
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: convertXdHeightToScreen(xdHeight: 14, deviceHeight: deviceHeight, xdScreenHeight: 680),),

              AuthInputTextField(placeholder: "Head Circumference",
                inputType: InputType.number,
                controller: _headCircumferenceController!,
                label: "Head Circumference",
                isRequired: false,
                errorText: null,
                isAuth: false,
                suffix: Container(
                    color: primary.withOpacity(0.1),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("cms",style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),),
                    )
                ),
              ),
              SizedBox(height: convertXdHeightToScreen(xdHeight: 14 , deviceHeight: deviceHeight, xdScreenHeight: 680),),

              AuthInputTextField(placeholder: "Require Birth Resucitation?",
                inputType: InputType.text,
                controller: _RequireBirthResucitationController!,
                label: "Require Birth Resucitation?",
                isRequired: false,
                errorText: null,
                isAuth: false,
              ),
              SizedBox(height: convertXdHeightToScreen(xdHeight: 14, deviceHeight: deviceHeight, xdScreenHeight: 680),),
              AuthInputTextField(placeholder: "Birth Notes",
                inputType: InputType.text,
                controller: _BirthNotesController!,
                label: "Birth Notes",
                isRequired: false,
                errorText: null,
                isAuth: true,
                minLines: 4,
                maxLines: 6,

              ),
              SizedBox(height: convertXdHeightToScreen(xdHeight: 14, deviceHeight: deviceHeight, xdScreenHeight: 680),),
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: MainButton(deviceWidth: deviceWidth, deviceHeight: deviceHeight, title: "Load from ECEB",
                      onPressed: (){

                    },
                    buttonSize: Size(
                      convertXdWidthToScreen(xdWidth: 136, deviceWidth: deviceWidth, xdScreenWidth: 360),
                      convertXdHeightToScreen(xdHeight: 30, deviceHeight: deviceHeight, xdScreenHeight: 680),
                    ),
                    textSize: 14,
                      textColor: Colors.white,
                      buttonColor: primary,
                      rounded: false,
                    ),
                  ),
                  const SizedBox(width: 8,),
                  Expanded(
                    flex: 1,
                    child: MainButton(deviceWidth: deviceWidth, deviceHeight: deviceHeight, title: "Save",
                      onPressed: (){
                        // on success
                        showAdaptiveDialog(context: context, builder: (context){
                          return SuccessDialog();
                        },);
                      },
                      buttonSize: Size(
                        convertXdWidthToScreen(xdWidth: 136, deviceWidth: deviceWidth, xdScreenWidth: 360),
                        convertXdHeightToScreen(xdHeight: 30, deviceHeight: deviceHeight, xdScreenHeight: 680),
                      ),
                      textSize: 14,
                      textColor: Colors.white,
                      buttonColor: Colors.green,
                      rounded: false,
                    ),
                  ),
                  const SizedBox(width: 8,),
                  Expanded(
                    flex: 1,
                    child: MainButton(deviceWidth: deviceWidth, deviceHeight: deviceHeight, title: "Discard",
                      onPressed: (){
                          Navigator.pop(context);
                      },
                      buttonSize: Size(
                        convertXdWidthToScreen(xdWidth: 136, deviceWidth: deviceWidth, xdScreenWidth: 360),
                        convertXdHeightToScreen(xdHeight: 30, deviceHeight: deviceHeight, xdScreenHeight: 680),
                      ),
                      textSize: 14,
                      textColor: Colors.white,
                      buttonColor: Colors.red,
                      rounded: false,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

