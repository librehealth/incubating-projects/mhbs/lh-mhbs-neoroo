import 'package:flutter/material.dart';
import 'package:neoroo/utils/constants.dart';

class SuccessDialog extends StatelessWidget {
  const SuccessDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0),
      child: AlertDialog(
        titlePadding: EdgeInsets.all(16),
        title: Align(
          alignment: Alignment.centerRight,
          child: GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: CircleAvatar(
              radius: 12,
              child: Icon(Icons.close,color: Colors.white, size: 15,),
              backgroundColor: primary,
              foregroundColor: Colors.white,
            ),
          ),
        ),
        contentPadding: EdgeInsets.only(right: 32, left: 32, top: 16, bottom: 32),

        content: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text("Baby Details Updated Successfully!",textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),),
            SizedBox(height: 16,),
            CircleAvatar(
              radius: 20,
              child: Icon(Icons.check,color: Colors.white, size: 30,),
              backgroundColor: primary,
              foregroundColor: Colors.white,
            ),

          ],
        ),

      ),
    );
  }
}
