import 'dart:developer';

import 'package:el_tooltip/el_tooltip.dart';
import 'package:flutter/material.dart';
import 'package:neoroo/screens/notifications_screen/notifications_screen.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/screens/shared_widgets/vital_chart.dart';
import 'package:neoroo/screens/temperature_screen/components/weekly_calendar.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:toggle_switch/toggle_switch.dart';

import '../temperature_screen/components/temperature_chart.dart';
import 'package:neoroo/routing/routing.dart' as router;

class HeartRateScreen extends StatefulWidget {
  const HeartRateScreen({Key? key}) : super(key: key);

  @override
  State<HeartRateScreen> createState() => _HeartRateScreenState();
}

class _HeartRateScreenState extends State<HeartRateScreen> {
  late TooltipBehavior  _tooltipBehavior;
  int selected= DateTime.now().day;
  @override
  void initState() {
    // TODO: implement initState

    _tooltipBehavior = TooltipBehavior(enable: true,

    );
    selected = DateTime.now().day;
    getCurrentDate();
    super.initState();
  }
  DateTime finalDate = DateTime.now();

  getCurrentDate(){

    final now = DateTime.now();

    var date = DateTime(now.year, now.month+1, 0).toString();

    var dateParse = findFirstDateOfTheWeek(now);


    setState(() {

      finalDate = dateParse ;

    });

  }

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday));
  }

  @override
  Widget build(BuildContext context) {

    final List<ChartData> chartData = [

      ChartData(DateTime(2023, 1, 1, 1), 29.6),
      ChartData(DateTime(2023, 1, 1, 1, 30), 28),
      ChartData(DateTime(2023, 1, 1, 2), 29),
      ChartData(DateTime(2023, 1, 1, 2, 30), 25),
      ChartData(DateTime(2023, 1, 1, 3), 20),
      ChartData(DateTime(2023, 1, 1, 3, 30), 70),
      ChartData(DateTime(2023, 1, 1, 4), 65),
      ChartData(DateTime(2023, 1, 1, 4, 30), 72),
      ChartData(DateTime(2023, 1, 1, 5), 61),
      ChartData(DateTime(2023, 1, 1, 5, 30), 62),
      ChartData(DateTime(2023, 1, 1, 6), 85),
      ChartData(DateTime(2023, 1, 1, 6, 30), 90),
      ChartData(DateTime(2023, 1, 1, 7), 77),
      ChartData(DateTime(2023, 1, 1, 7, 30), 95),
      ChartData(DateTime(2023, 1, 1, 8), 130),
      ChartData(DateTime(2023, 1, 1, 8, 30), 150),
      ChartData(DateTime(2023, 1, 1, 9), 122),
      ChartData(DateTime(2023, 1, 1, 9, 30), 135),
      ChartData(DateTime(2023, 1, 1, 10), 110),
      ChartData(DateTime(2023, 1, 1, 10, 30), 100),
      ChartData(DateTime(2023, 1, 1, 11), 90),
      ChartData(DateTime(2023, 1, 1, 12), 115),
      ChartData(DateTime(2023, 1, 1, 12, 30), 55),
      ChartData(DateTime(2023, 1, 1, 13), 25),
      ChartData(DateTime(2023, 1, 1, 13, 30), 85),
      ChartData(DateTime(2023, 1, 1, 14), 28),
      ChartData(DateTime(2023, 1, 1, 14, 30), 22),
      ChartData(DateTime(2023, 1, 1, 15), 35),
      ChartData(DateTime(2023, 1, 1, 15, 30), 35.5),
      ChartData(DateTime(2023, 1, 1, 16), 31.2),
      ChartData(DateTime(2023, 1, 1, 16, 30), 36.5),
      ChartData(DateTime(2023, 1, 1, 17), 37),
    ];
    chartData.sort((a,b)=>a.x.compareTo(b.x));
    double deviceWidth = MediaQuery.of(context).size.width;
    return  Scaffold(
      backgroundColor: Colors.white,
      appBar: ScreenHeader(
        isUserIconVisible: false,
        title: "Heart Rate",
        hasBackButton: true,
        backButtonOnPressed: (){
          Navigator.pop(context);
        },
        actionIconImages: [
          "assets/images/notification.png",
          "assets/images/menu.png"
        ],
        actionIconOnPressFunctions: [
              () {
                pushScreen(context, screen: NotificationsScreen(), settings: RouteSettings(name: router.notificationsPage));

              },
              (){
            getCurrentDate.call();
          }
        ],
        hasSearchBar: false,

      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 8,),
            WeeklyCalendar(finalDate: finalDate, onDateChange: (){}),
            VitalChart(chartData: chartData,
              normalBegin: 60,
              normalEnd: 120,
              highBegin: 120,
              highEnd: 250,
              lowBegin: 0,
              lowEnd: 60,
              unit: "bpm",
              title: "Heart Rate",
              highAlertImage: "assets/images/heart_high_rate.png",
              lowAlertImage: "assets/images/heart_low_rate.png",)
          ],
        ),
      ),
    );
  }
}


