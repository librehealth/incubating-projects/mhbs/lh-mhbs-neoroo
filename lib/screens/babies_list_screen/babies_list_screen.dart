import 'package:flutter/material.dart';
import 'package:neoroo/screens/add_edit_baby_details/add_edit_baby_details.dart';
import 'package:neoroo/screens/landing_screen/components/main_button.dart';
import 'package:neoroo/screens/notifications_screen/notifications_screen.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/screens/vitals_list_screen/components/baby_details.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';
import 'package:neoroo/routing/routing.dart' as router;
class BabiesListScreen extends StatefulWidget {
  const BabiesListScreen({Key? key}) : super(key: key);

  @override
  State<BabiesListScreen> createState() => _VitalsListScreenState();
}

class _VitalsListScreenState extends State<BabiesListScreen> {
  late TextEditingController searchController;
  final List<Map<String, String>> babyDetails= [
    {"name": "Mila", "birthDate": "20/07/2020","weight":"1500 gm", "birthTime":"9:30 AM", "imageURL":"https://media.istockphoto.com/id/636253690/photo/newborn-baby-girl-sleeping-in-bowl.jpg?s=612x612&w=0&k=20&c=XUvinWRoz4oyXdPEoMcEUgNJMAlxjLBr8fwlSHbX8TU="},
    {"name": "Nia", "birthDate": "20/07/2020","weight":"1500 gm", "birthTime":"9:30 AM", "imageURL":"https://media.istockphoto.com/id/636253690/photo/newborn-baby-girl-sleeping-in-bowl.jpg?s=612x612&w=0&k=20&c=XUvinWRoz4oyXdPEoMcEUgNJMAlxjLBr8fwlSHbX8TU="},
    {"name": "Nia", "birthDate": "20/07/2020", "weight":"1500 gm","birthTime":"9:30 AM", "imageURL":"https://media.istockphoto.com/id/636253690/photo/newborn-baby-girl-sleeping-in-bowl.jpg?s=612x612&w=0&k=20&c=XUvinWRoz4oyXdPEoMcEUgNJMAlxjLBr8fwlSHbX8TU="},
    {"name": "Nia", "birthDate": "20/07/2020", "weight":"1500 gm","birthTime":"9:30 AM", "imageURL":"https://media.istockphoto.com/id/636253690/photo/newborn-baby-girl-sleeping-in-bowl.jpg?s=612x612&w=0&k=20&c=XUvinWRoz4oyXdPEoMcEUgNJMAlxjLBr8fwlSHbX8TU="},
  ];
  @override
  void initState() {
    super.initState();
    searchController = TextEditingController();
  }
  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    final deviceHeight= MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: ScreenHeader(
          isUserIconVisible: false,
          title: "Baby's List",
          actionIconImages: [
            "assets/images/notification.png",
          ],
          actionIconOnPressFunctions: [
                () {
                  pushScreen(context, screen: NotificationsScreen(), settings: RouteSettings(name: router.notificationsPage));

                },
          ],
          hasSearchBar: true,
          searchController: searchController,
          searchHintText: "Type moms’ name or choose birth date",
          onSearchTextChanged: (text) {
            setState(() {
              // search text
            });
          },
          hasBackButton: true,
          backButtonOnPressed: () {
            Navigator.pop(context);
          },
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Column(
                  children: babyDetails.map((e) =>
                      BabyDetailsCard(
                        name: e["name"]!,
                        buttonText: "View and Edit",
                        birthDate: e["birthDate"]!,
                        birthTime: e["birthTime"]!,
                        weight: e["weight"]!,
                        imageURL: e["imageURL"]!,
                        onButtonPress: (){
                          pushScreen(context, screen: AddEditBabyDetails(), settings: RouteSettings(name: router.addOrEditBabyPage));
                        },
                      )).toList(
                  )
              ),
              SizedBox(height: convertXdHeightToScreen(xdHeight: 30, deviceHeight: deviceHeight, xdScreenHeight: 696),),
              MainButton(deviceWidth: deviceWidth, deviceHeight: deviceHeight, title: "Add Baby Details", 
                  onPressed: (){
                    pushScreen(context, screen: AddEditBabyDetails(), settings: RouteSettings(name: router.addOrEditBabyPage));
                  },
                  textSize: 12,
                  rounded: false,
                  buttonSize:Size(
                    convertXdWidthToScreen(xdWidth: 127, deviceWidth: deviceWidth, xdScreenWidth: 380),
                    convertXdHeightToScreen(xdHeight: 31, deviceHeight: deviceHeight, xdScreenHeight: 696),
                  ) ),
              SizedBox(height: convertXdHeightToScreen(xdHeight: 40, deviceHeight: deviceHeight, xdScreenHeight: 696),),

            ],
          ),
        )
    );
  }
}

