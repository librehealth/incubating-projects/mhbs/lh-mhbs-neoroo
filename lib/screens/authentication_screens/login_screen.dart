import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:neoroo/bloc/authentication/login_bloc/login_bloc.dart';
import 'package:neoroo/bloc/authentication/login_bloc/login_bloc_events.dart';
import 'package:neoroo/bloc/authentication/login_bloc/login_bloc_states.dart';
import 'package:neoroo/screens/authentication_screens/components/auth_header.dart';
import 'package:neoroo/screens/authentication_screens/components/auth_input_text_field.dart';
import 'package:neoroo/screens/authentication_screens/components/forgot_password.dart';
import 'package:neoroo/screens/authentication_screens/components/remember_me_check_box.dart';
import 'package:neoroo/screens/authentication_screens/components/welcome_widget.dart';
import 'package:neoroo/screens/authentication_screens/signup_screen.dart';
import 'package:neoroo/screens/landing_screen/components/main_button.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:neoroo/utils/vertical_space.dart';
import 'package:neoroo/routing/routing.dart' as router;

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key, required this.userType}) : super(key: key);
  final UserType userType;

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TextEditingController _usernameController;
  late TextEditingController _passwordController;
  late TextEditingController _serverURLController;
  bool allFieldsAreFilled = false;
  bool _isLoading = false;
  String userErrorText = "";
  String passwordErrorText = "";
  String serverURLErrorText = "";
  bool rememberMe = false;

  @override
  void initState() {
    super.initState();
    _usernameController = TextEditingController();
    _passwordController = TextEditingController();
    _serverURLController = TextEditingController();
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    _serverURLController.dispose();
    super.dispose();
  }

  bool validateLoginInput(String username, String password, String serverURL) {
    bool valid = true;
    if (username.isEmpty) {
      setState(() {
        userErrorText = "This field is missing";
      });
      valid = false;
    }
    if (password.isEmpty) {
      setState(() {
        passwordErrorText = "This field is missing";
      });
      valid = false;
    }
    if (serverURL.isEmpty) {
      setState(() {
        serverURLErrorText = "This field is missing";
      });
      valid = false;
    }
    return valid;
  }

  void onChanged(String value) {
    if (_usernameController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty &&
        _serverURLController.text.isNotEmpty) {
      setState(() {
        allFieldsAreFilled = true;
      });
    } else {
      setState(() {
        allFieldsAreFilled = false;
      });
    }
    setState(() {
      userErrorText = "";
      passwordErrorText = "";
      serverURLErrorText = "";
    });
  }

  @override
  Widget build(BuildContext context) {
    double deviceHeight = MediaQuery.of(context).size.height;
    double deviceWidth = MediaQuery.of(context).size.width;

    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginLoading) {
          setState(() {
            _isLoading = true;
          });
        } else if (state is LoginLoaded) {
          setState(() {
            _isLoading = false;
          });
          Navigator.pushNamed(context, router.chooseOrganisationPage);
        } else if (state is LoginGeneralError) {
          setState(() {
            _isLoading = false;
          });
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(state.exception.message),
              backgroundColor: red,
            ),
          );
        }
      },
      child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                VerticalSpace(
                  height: convertXdHeightToScreen(
                    xdHeight: 34,
                    deviceHeight: deviceHeight,
                  ),
                ),
                AuthenticationHeader(
                  deviceWidth: deviceWidth,
                  deviceHeight: deviceHeight,
                ),
                VerticalSpace(
                  height: convertXdHeightToScreen(
                    xdHeight: 60,
                    deviceHeight: deviceHeight,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: convertXdWidthToScreen(
                      xdWidth: 46,
                      deviceWidth: deviceWidth,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      WelcomeWidget(
                        deviceWidth: deviceWidth,
                        deviceHeight: deviceHeight,
                        userRole: widget.userType,
                      ),
                      VerticalSpace(
                        height: convertXdHeightToScreen(
                          xdHeight: 65,
                          deviceHeight: deviceHeight,
                        ),
                      ),
                      AuthInputTextField(
                        placeholder: "",
                        inputType: InputType.serverURL,
                        controller: _serverURLController,
                        label: "Server URL",
                        isRequired: true,
                        errorText: serverURLErrorText.isEmpty
                            ? null
                            : serverURLErrorText,
                        onChanged: onChanged,
                      ),
                      VerticalSpace(
                        height: convertXdHeightToScreen(
                          xdHeight: 42,
                          deviceHeight: deviceHeight,
                        ),
                      ),
                      AuthInputTextField(
                        placeholder: "",
                        inputType: InputType.username,
                        controller: _usernameController,
                        label: "User Name",
                        isRequired: true,
                        errorText:
                            userErrorText.isEmpty ? null : userErrorText,
                        onChanged: onChanged,
                      ),
                      VerticalSpace(
                        height: convertXdHeightToScreen(
                          xdHeight: 42,
                          deviceHeight: deviceHeight,
                        ),
                      ),
                      AuthInputTextField(
                        placeholder: "",
                        inputType: InputType.password,
                        controller: _passwordController,
                        label: "Password",
                        isRequired: true,
                        errorText: passwordErrorText.isEmpty
                            ? null
                            : passwordErrorText,
                        onChanged: onChanged,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: convertXdWidthToScreen(
                      xdWidth: 46,
                      deviceWidth: deviceWidth,
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      RememberMeCheckBox(
                        onChecked: () {
                          setState(() {
                            rememberMe = true;
                          });
                        },
                        onUnChecked: () {
                          setState(() {
                            rememberMe = false;
                          });
                        },
                      ),
                      ForgotPasswordHyperLink(
                        forgotPasswordLink: "https://www.google.com",
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: convertXdWidthToScreen(
                      xdWidth: 34,
                      deviceWidth: deviceWidth,
                    ),
                    right: convertXdWidthToScreen(
                      xdWidth: 46,
                      deviceWidth: deviceWidth,
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      MainButton(
                        deviceWidth: deviceWidth,
                        deviceHeight: deviceHeight,
                        title: _isLoading ? "Signing in..." : "SignIn",
                        onPressed: _isLoading
                            ? null
                            : () {
                                if (validateLoginInput(
                                  _usernameController.text,
                                  _passwordController.text,
                                  _serverURLController.text,
                                )) {
                                  context.read<LoginBloc>().add(
                                        LoginEvent(
                                          _serverURLController.text.trim(),
                                          _passwordController.text,
                                          _usernameController.text.trim(),
                                        ),
                                      );
                                }
                              },
                        buttonSize: Size(
                          convertXdWidthToScreen(
                            xdWidth: 113,
                            deviceWidth: deviceWidth,
                          ),
                          convertXdHeightToScreen(
                            xdHeight: 38,
                            deviceHeight: deviceHeight,
                          ),
                        ),
                        buttonColor: allFieldsAreFilled ? primary : secondary,
                        textColor:
                            allFieldsAreFilled ? Colors.white : Colors.black,
                        textSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                      const SizedBox(width: 14),
                      MainButton(
                        deviceWidth: deviceWidth,
                        deviceHeight: deviceHeight,
                        title: "SignUp",
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  SignUpScreen(widget.userType),
                            ),
                          );
                        },
                        buttonSize: Size(
                          convertXdWidthToScreen(
                            xdWidth: 113,
                            deviceWidth: deviceWidth,
                          ),
                          convertXdHeightToScreen(
                            xdHeight: 38,
                            deviceHeight: deviceHeight,
                          ),
                        ),
                        buttonColor: secondary,
                        textColor: Colors.black,
                        textSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}