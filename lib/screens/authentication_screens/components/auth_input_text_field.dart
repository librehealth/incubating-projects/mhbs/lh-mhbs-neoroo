import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/methods.dart';
import '../../../utils/enums.dart';

class AuthInputTextField extends StatefulWidget {
  /// This is the widget used to create the input text field used in the authentication screens.
  /// It takes [placeholder] as the placeholder text, [inputType] as the type of input whether mail, username, or password,
  /// [controller] as the controller for the text field, [onTap] as the function that will be called when the text field is tapped,
  /// [onSubmitted] as the function that will be called when the text field is submitted,
  /// [onChanged] as the function that will be called whenever the user changes the text in the text field,
  /// [label] as the label text of the text field,
  /// [isRequired] as a boolean that determines whether the text field is required or not,
  /// [errorText] as the error text that will be displayed if there's an error in the text that the user entered.
  const AuthInputTextField({
    Key? key, 
    required this.placeholder, 
    required this.inputType, 
    required this.controller, 
    this.onTap, 
    this.onSubmitted, 
    required this.label, 
    required this.isRequired, 
    required this.errorText,
    this.suffix, 
    this.isAuth = true,
    this.maxLines = 1,
    this.minLines = 1,
    this.onChanged,
    this.enabled = true,
  }) : super(key: key);

  final String placeholder;
  final InputType inputType;
  final TextEditingController controller;
  final VoidCallback? onTap;
  final ValueChanged<String>? onSubmitted;
  final String label;
  final bool isRequired;
  final String? errorText;
  final ValueChanged<String>? onChanged;
  final Widget? suffix;
  final bool? isAuth;
  final int? maxLines;
  final int? minLines;
  final bool enabled;

  @override
  State<AuthInputTextField> createState() => _AuthInputTextFieldState();
}

class _AuthInputTextFieldState extends State<AuthInputTextField> {
  bool isFocused = false;
  bool _obscureText = true;

  /// Validates input based on [InputType] and returns an error message if invalid.
  /// For [InputType.serverURL], checks that URL starts with 'http://' or 'https://'.
  /// For [InputType.password], checks if the password is at least 6 characters long.
  /// For [InputType.email], checks for a valid email format.
  String? getValidationError(String value) {
    if (value.isEmpty && widget.isRequired) {
      return "${widget.label} is required";
    }
    
    switch (widget.inputType) {
      case InputType.serverURL:
        if (!value.startsWith('http://') && !value.startsWith('https://')) {
          return "URL must start with http:// or https://";
        }
        break;
      case InputType.password:
        if (value.isNotEmpty && value.length < 6) {
          return "Password must be at least 6 characters";
        }
        break;
      case InputType.email:
        final emailRegExp = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
        if (value.isNotEmpty && !emailRegExp.hasMatch(value)) {
          return "Invalid email format";
        }
        break;
      default:
        break;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    
    return Material(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: SizedBox(
            height: !widget.isAuth! 
              ? convertXdHeightToScreen(xdHeight: 48, deviceHeight: deviceHeight, xdScreenHeight: 680)
              : null,
            child: TextField(
              maxLines: widget.inputType == InputType.password ? 1 : widget.maxLines,
              minLines: widget.minLines,
              onChanged: (value) {
                if (widget.onChanged != null) {
                  widget.onChanged!(value);
                }
                // Trigger validation on change
                setState(() {
                  getValidationError(value);
                });
              },
              onTap: () {
                setState(() {
                  isFocused = true;
                });
                if (widget.onTap != null) {
                  widget.onTap!.call();
                }
              },
              onSubmitted: (value) {
                setState(() {
                  isFocused = false;
                });
                if (widget.onSubmitted != null) {
                  widget.onSubmitted!.call(value);
                }
              },
              enabled: widget.enabled,
              cursorColor: Colors.black87,
              cursorWidth: 1,
              obscureText: widget.inputType == InputType.password ? _obscureText : false,
              controller: widget.controller,
              obscuringCharacter: '•',
              keyboardType: _getKeyboardType(),
              decoration: InputDecoration(
                suffix: widget.inputType == InputType.password 
                  ? IconButton(
                      icon: Icon(
                        _obscureText ? Icons.visibility : Icons.visibility_off,
                        color: primary,
                        size: 20,
                      ),
                      onPressed: () {
                        setState(() {
                          _obscureText = !_obscureText;
                        });
                      },
                    )
                  : widget.suffix,
                floatingLabelBehavior: FloatingLabelBehavior.always,
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(0),
                  borderSide: BorderSide(
                    color: primary,
                    width: 1,
                  ),
                ),
                errorText: widget.errorText,
                hintText: widget.placeholder,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(0),
                  borderSide: BorderSide(
                    color: primary,
                    width: 1,
                  ),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(0),
                  borderSide: BorderSide(
                    color: primary,
                    width: 1,
                  ),
                ),
                label: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      '${widget.label} ',
                      style: TextStyle(
                        color: primary,
                        fontSize: 18,
                      ),
                    ),
                    Text(
                      widget.isRequired ? '*' : "",
                      style: const TextStyle(
                        color: Colors.red,
                        fontSize: 18,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  /// Returns the appropriate keyboard type based on [InputType]
  TextInputType _getKeyboardType() {
    switch (widget.inputType) {
      case InputType.email:
        return TextInputType.emailAddress;
      case InputType.number:
        return TextInputType.number;
      case InputType.datetime:
        return TextInputType.datetime;
      default:
        return TextInputType.text;
    }
  }
}
