import 'package:flutter/material.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:neoroo/utils/methods.dart';
import '../../../utils/constants.dart';

class WelcomeWidget extends StatefulWidget {
  const WelcomeWidget({
    Key? key,
    required this.deviceWidth,
    required this.deviceHeight,
    required this.userRole,
    this.username,
    this.animate = true,
  }) : super(key: key);

  final double deviceWidth;
  final double deviceHeight;
  final UserType userRole;
  final String? username;
  final bool animate;

  @override
  State<WelcomeWidget> createState() => _WelcomeWidgetState();
}

class _WelcomeWidgetState extends State<WelcomeWidget> 
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _fadeIn;
  late Animation<Offset> _slideIn;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 800),
    );

    _fadeIn = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOut,
    ));

    _slideIn = Tween<Offset>(
      begin: const Offset(0, 0.2),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOut,
    ));

    if (widget.animate) {
      _controller.forward();
    } else {
      _controller.value = 1.0;
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  String get _welcomeMessage {
    if (widget.username?.isNotEmpty ?? false) {
      return "Welcome back, ${widget.username}";
    }
    return "Welcome";
  }

  double get _fontSize {
    if ((widget.deviceWidth > widget.deviceHeight && widget.deviceWidth > 600) 
        || widget.deviceWidth > 960) {
      return 32;
    }
    return 22;
  }

  double get _iconSize {
    if ((widget.deviceWidth > widget.deviceHeight && widget.deviceWidth > 600) 
        || widget.deviceWidth > 960) {
      return convertXdWidthToScreen(
        xdWidth: 34,
        deviceWidth: widget.deviceHeight,
      );
    }
    return convertXdHeightToScreen(
      xdHeight: 34,
      deviceHeight: widget.deviceHeight,
    );
  }

  String get _userTypeImage {
    return widget.userRole == UserType.familyMember 
        ? "assets/images/family_member_icon.png"
        : "assets/images/care_provider.png";
  }

  String get _userTypeText {
    return widget.userRole == UserType.familyMember 
        ? "Family Member"
        : "Care Provider";
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: _fadeIn,
      child: SlideTransition(
        position: _slideIn,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  _welcomeMessage,
                  style: TextStyle(
                    fontSize: _fontSize,
                    fontWeight: FontWeight.w500,
                    color: primary,
                  ),
                ),
                SizedBox(
                  width: (widget.deviceWidth > widget.deviceHeight && 
                         widget.deviceWidth > 600) || 
                         widget.deviceWidth > 960 
                      ? 10 
                      : convertXdWidthToScreen(
                          xdWidth: 30,
                          deviceWidth: widget.deviceWidth,
                        ),
                ),
                Hero(
                  tag: 'user_type_icon',
                  child: Image.asset(
                    _userTypeImage,
                    width: (widget.deviceWidth > widget.deviceHeight && 
                           widget.deviceWidth > 600) || 
                           widget.deviceWidth > 960 
                        ? _iconSize
                        : null,
                    height: (widget.deviceWidth > widget.deviceHeight && 
                            widget.deviceWidth > 600) || 
                            widget.deviceWidth > 960 
                        ? null
                        : _iconSize,
                    errorBuilder: (context, error, stackTrace) {
                      return Icon(
                        widget.userRole == UserType.familyMember
                            ? Icons.family_restroom
                            : Icons.medical_services,
                        size: _iconSize,
                        color: primary,
                      );
                    },
                  ),
                ),
              ],
            ),
            const SizedBox(height: 4),
            Text(
              _userTypeText,
              style: TextStyle(
                fontSize: _fontSize * 0.6,
                color: secondaryGrey,
                fontWeight: FontWeight.w400,
              ),
            ),
          ],
        ),
      ),
    );
  }
}