import 'package:flutter/material.dart';
import 'package:neoroo/screens/authentication_screens/components/radio_list_tile.dart';
import 'package:neoroo/screens/landing_screen/components/main_button.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:neoroo/routing/routing.dart' as router;

class ChooseOrgScreen extends StatefulWidget {
  const ChooseOrgScreen({Key? key}) : super(key: key);

  @override
  State<ChooseOrgScreen> createState() => _ChooseOrgScreenState();
}

class _ChooseOrgScreenState extends State<ChooseOrgScreen> {
  int selectedOrganization = 1;
  final List<Map<String, dynamic>> organizations =
      [
        {
          "name": "Organization 1",
          "details": "Details of Organization 1",
          "value": 1
        },
        {
          "name": "Organization 2",
          "details": "Details of Organization 2",
          "value": 2
        },
        {
          "name": "Organization 3",
          "details": "Details of Organization 3",
          "value": 3
        },
        {
          "name": "Organization 3",
          "details": "Details of Organization 3",
          "value": 4
        },
        {
          "name": "Organization 3",
          "details": "Details of Organization 3",
          "value": 5
        },
        {
          "name": "Organization 3",
          "details": "Details of Organization 3",
          "value": 6
        },
        {
          "name": "Organization 3",
          "details": "Details of Organization 3",
          "value": 7
        },
        {
          "name": "Organization 3",
          "details": "Details of Organization 3",
          "value": 8
        },
        {
          "name": "Organization 3",
          "details": "Details of Organization 3",
          "value": 9
        },
        {
          "name": "Organization 3",
          "details": "Details of Organization 3",
          "value": 10
        },
      ];
  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: ScreenHeader(title: "Choose Organization", isUserIconVisible: false,),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 16,),
          Expanded(
            child: SingleChildScrollView(

              child: Column(
                children: organizations.map((e) => RadioListTileCustom(
                  title: e["name"],
                  subtitle: e["details"],
                  value: e["value"],
                  groupValue: selectedOrganization,
                  onChanged: (value){
                    setState(() {
                      selectedOrganization = value!;
                    });
                  },
                  onTap: (){
                    setState(() {
                      selectedOrganization = e["value"];
                    });
                  },
                )).toList(),
              ),
            ),
          ),
          SizedBox(height: 16,),
          Padding(
            padding: const EdgeInsets.only(right: 32.0, bottom: 32.0),
            child: Align(alignment: Alignment.centerRight,
            child: MainButton(
              title: "Proceed",
              textSize: 20,
              textColor: Colors.white,
              fontWeight: FontWeight.w500,
              deviceWidth: deviceWidth,
              deviceHeight: deviceHeight,
              buttonSize: Size(convertXdWidthToScreen(xdWidth: 110, deviceWidth: deviceWidth, xdScreenWidth: 360),
                  convertXdHeightToScreen(xdHeight: 35, deviceHeight: deviceHeight, xdScreenHeight: 667)),
              onPressed: (){
                // TODO(Mehul): functionality of signing in
                // push the named route homePage and pop all the routes before it
                Navigator.of(context)
                    .popUntil((route) => false);
                Navigator.of(context).pushNamed(router.mainNavScreen);

              },
            ),
            ),
          )
        ],
      )
    );
  }
}

