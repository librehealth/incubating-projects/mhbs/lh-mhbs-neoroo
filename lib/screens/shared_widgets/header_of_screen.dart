import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:neoroo/utils/methods.dart';

// make it a preferred sized widget
class ScreenHeader extends StatefulWidget implements PreferredSizeWidget  {


  /// This is the header of the screen. It contains the title of the screen, user icon, and action icons.
  const ScreenHeader({Key? key, this.backgroundColor = primary,
    required this.title,
    this.isUserIconVisible=true,
    this.toolBarIconsColor=ToolbarIconsTheme.light,
    this.userImage="assets/images/Everlyne_profile.png",  this.actionIconImages= const <String>[],  this.actionIconOnPressFunctions = const <VoidCallback>[],
    this.height = 80,
    this.hasBackButton = false,
    this.backButtonOnPressed,
    this.hasSearchBar = false,
    this.searchHintText = "Search",
    this.searchController, this.onSearchTextChanged,
    this.subtitle,
    this.hasMenuButton = false,
    this.menuItems = const <String>[],
    this.menuItemOnPressFunctions = const <VoidCallback>[],
    this.menuIcons = const <String>[],
  }) : super(key: key);
  final Color backgroundColor;
  final String title;
  final bool isUserIconVisible;
  final String userImage;
  final ToolbarIconsTheme toolBarIconsColor;
  // list of action icon images
  final List<String> actionIconImages;
  // list of action icon on press functions
  final List<VoidCallback> actionIconOnPressFunctions;
  final double height;
  final bool hasBackButton;
  final VoidCallback? backButtonOnPressed;
  final bool hasSearchBar;
  final String searchHintText;
  final TextEditingController? searchController;
  final Function(String)? onSearchTextChanged;
  final String? subtitle;
  final bool hasMenuButton;
  final List<String> menuItems;
  final List<VoidCallback> menuItemOnPressFunctions;
  final List<String> menuIcons;
  @override
  State<ScreenHeader> createState() => _ScreenHeaderState();

  @override
  Size get preferredSize => Size.fromHeight(hasSearchBar?height+60:subtitle!=null?height:height);
}

class _ScreenHeaderState extends State<ScreenHeader> {
  bool startSearching = false;
  List<Widget> actionWidgets = [];
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    double deviceHeight= MediaQuery.of(context).size.height;
    double deviceWidth = MediaQuery.of(context).size.width;
    actionWidgets = widget.hasMenuButton?[
      PopupMenuButton(
        child: Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Center(child: Icon(FontAwesomeIcons.ellipsisVertical,color: Colors.white, size: 20,)),
        ),
        position: PopupMenuPosition.under,
        splashRadius: 0,
        padding: EdgeInsets.zero,
        itemBuilder: (context) {
          return widget.menuItems.map((e) => PopupMenuItem(
            onTap: (){
              widget.menuItemOnPressFunctions[widget.menuItems.indexOf(e)].call();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const SizedBox(width: 10,),
                Image.asset(widget.menuIcons[widget.menuItems.indexOf(e)], width: 20, height: 20,),
                const SizedBox(width: 20,),
                Expanded(child: Text(e)),
              ],
            ),)).toList();
        },
      ),
    ]:[];
    actionWidgets.insertAll(0,widget.actionIconImages.map((actionImage) => IconButton(
      onPressed: () {
        if (widget.actionIconImages.indexOf(actionImage)>= widget.actionIconOnPressFunctions.length) return;
        widget.actionIconOnPressFunctions[widget.actionIconImages.indexOf(actionImage)].call();
      },
      icon: Padding(
        padding: EdgeInsets.all((deviceWidth > deviceHeight && deviceWidth > 600)
            || deviceWidth >960?8.0:0),
        child: Image.asset(actionImage,
          width:(deviceWidth > deviceHeight && deviceWidth > 600)
              || deviceWidth > 960 ? null: convertXdWidthToScreen(xdWidth: 16, deviceWidth: deviceWidth, xdScreenWidth: 360),
        ),
      ),
      padding:(deviceWidth > deviceHeight && deviceWidth > 600)
          || deviceWidth > 960 ?EdgeInsets.only(right: 12.0, top: 12.0, bottom: 12.0): EdgeInsets.only(right: 16.0, top: 16.0, bottom: 16.0),
      iconSize: (deviceWidth > deviceHeight && deviceWidth > 600)
          || deviceWidth > 960? 24: 16,
    )).toList(),);

    return AppBar(
          systemOverlayStyle: SystemUiOverlayStyle(
            // Status bar color
            statusBarColor: widget.backgroundColor,

            // Status bar brightness (optional)
            statusBarIconBrightness: widget.toolBarIconsColor==ToolbarIconsTheme.light? Brightness.light: Brightness.dark, // For Android (dark icons)
            statusBarBrightness:widget.toolBarIconsColor==ToolbarIconsTheme.light?  Brightness.dark: Brightness.light, // For iOS (dark icons)
          ),
          backgroundColor: widget.backgroundColor,
          toolbarHeight: (deviceWidth > deviceHeight && deviceWidth > 600)
              || deviceWidth > 960 ? widget.height-16:convertXdHeightToScreen(xdHeight: widget.preferredSize.height, deviceHeight: deviceHeight),
          title:widget.subtitle!=null? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.title,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.w500
              ),
              ),
              const SizedBox(height: 5,),
              Text(
                widget.subtitle!,
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w400
                ),),
            ],
          ): Text(
            widget.title,
            style: TextStyle(
                color: Colors.white,
                fontSize: (deviceWidth > deviceHeight && deviceWidth > 600)
                    || deviceWidth > 960 ?24:20,
                fontWeight: FontWeight.w500
            ),
          ),

          leading: widget.isUserIconVisible?Padding(
            padding:(deviceWidth > deviceHeight && deviceWidth > 600)
                || deviceWidth > 960 ?EdgeInsets.only(left: 4.0, top: 4.0, bottom: 4.0): EdgeInsets.only(left: 16.0, top: 16.0, bottom: 16.0),
            child: CircleAvatar(
                backgroundImage: AssetImage(widget.userImage),
                backgroundColor: primary,

            ),
          ):widget.hasBackButton?
          widget.subtitle!=null?Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 18.0),
              child: IconButton(
                onPressed: () {
                  widget.backButtonOnPressed!.call();
                },
                icon: Icon(Icons.arrow_back_ios_rounded, color: Colors.white, size: 20,),
                padding: EdgeInsets.all(10),
              ),
            ),
          ): IconButton(
            onPressed: () {
              widget.backButtonOnPressed!.call();
            },
            icon: Icon(Icons.arrow_back_ios_rounded, color: Colors.white, size: 20,),
            padding: EdgeInsets.all(10),
          )
              :Container(),
          leadingWidth: widget.isUserIconVisible?
          convertXdWidthToScreen(xdWidth: (deviceWidth > deviceHeight && deviceWidth > 600)
              || deviceWidth > 960 ? 30: 72, deviceWidth: deviceWidth, xdScreenWidth: 360):widget.hasBackButton?convertXdWidthToScreen(xdWidth: 20, deviceWidth: deviceWidth, xdScreenWidth: 360):0,
          actions:  actionWidgets,
      bottom: widget.hasSearchBar?PreferredSize(
        preferredSize: Size.fromHeight(68),
        child: Container(
          height: 60,
          color: widget.backgroundColor,
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
            child: Center(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(6),
                ),
                child: TextField(
                  controller: widget.searchController,
                  onChanged: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        startSearching = false;
                      });
                    } else {
                      setState(() {
                        startSearching = true;
                      });
                    }
                    if (widget.onSearchTextChanged != null) {
                      widget.onSearchTextChanged!.call(value);
                    }
                  },
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                  decoration: InputDecoration(
                    hintText: widget.searchHintText,
                    hintStyle: TextStyle(
                      color: Colors.white.withOpacity(0.6),
                      fontSize: 16,
                    ),
                    border: InputBorder.none,
                    prefixIcon: Icon(Icons.search, color: Colors.white,),
                    suffixIcon: startSearching?GestureDetector(child: Icon(Icons.close, color: Colors.white,), onTap: (){

                      widget.searchController!.clear();
                      setState(() {
                        startSearching = false;
                      });

                    },):null,
                  ),
                ),
              ),
            ),
          ),
        ),
      )
      :null,
    );
  }
}
