import 'dart:developer';

import 'package:el_tooltip/el_tooltip.dart';
import 'package:flutter/material.dart';
import 'package:neoroo/screens/temperature_screen/components/temperature_chart.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:syncfusion_flutter_charts/charts.dart';


class VitalChart extends StatefulWidget {
  const VitalChart({Key? key, required this.chartData, required this.normalBegin, required this.normalEnd, required this.highBegin, required this.highEnd, required this.lowBegin, required this.lowEnd, required this.unit, required this.title, required this.highAlertImage, required this.lowAlertImage}) : super(key: key);
  final List<ChartData> chartData;
  final double normalBegin;
  final double normalEnd;
  final double highBegin;
  final double highEnd;
  final double lowBegin;
  final double lowEnd;
  final String unit;
  final String title;
  final String highAlertImage;
  final String lowAlertImage;
  @override
  State<VitalChart> createState() => _VitalChartState();

}

class _VitalChartState extends State<VitalChart> {
  late TooltipBehavior  _tooltipBehavior;
  late ZoomPanBehavior _zoomPanBehavior;
  List<List<ChartData>> chartDataList = [];
  List<ChartData> lowList = [];
  List<ChartData> highList = [];
  @override
  void initState() {
    // TODO: implement initState
    _tooltipBehavior = TooltipBehavior(enable: true,
    canShowMarker: true,
      shouldAlwaysShow: true,

    );
    _zoomPanBehavior = ZoomPanBehavior(
      enablePinching: true,
      zoomMode: ZoomMode.x,
      enablePanning: true,
      enableDoubleTapZooming:  true,
    );
    widget.chartData.sort((a,b)=>a.x.compareTo(b.x));
    chartDataList = partitionVitalValues(widget.chartData);
    for (var i = 0; i < chartDataList.length; i++) {
      if(chartDataList[i][0].y>widget.highBegin){
        highList.add(getMedianHighVital(chartDataList[i]));
      }
      else if(chartDataList[i][0].y<widget.lowEnd){
        lowList.add(getMedianLowVital(chartDataList[i]));
      }
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final List<ChartData> chartData = widget.chartData;

    chartDataList = partitionVitalValues(widget.chartData);
    for (var i = 0; i < chartDataList.length; i++) {
      if(chartDataList[i][0].y>widget.highBegin){
        highList.add(getMedianHighVital(chartDataList[i]));
      }
      else if(chartDataList[i][0].y<widget.lowEnd){
        lowList.add(getMedianLowVital(chartDataList[i]));
      }
    }
    return  Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.title + " (" + widget.unit + ")",
            style: TextStyle(
              color: primary,
            ),
          ),
          SizedBox(
              width: MediaQuery.of(context).size.width*0.9,
              height: MediaQuery.of(context).size.height*0.4,
              child: SfCartesianChart(

                plotAreaBorderWidth: 0,
                primaryXAxis: DateTimeAxis(
                  labelPosition: ChartDataLabelPosition.outside,
                  title: AxisTitle(text: 'Time',
                      textStyle: TextStyle(
                          color: primary
                      ),
                      alignment: ChartAlignment.far),
                  axisLine: AxisLine(color: primary),
                  interval: 1,
                  plotBands: <PlotBand>[
                    PlotBand(
                        isVisible: true,
                        start: chartData[0].x,
                        end: DateTime(chartData[0].x.year, chartData[0].x.month, chartData[0].x.day, chartData[0].x.hour, chartData[0].x.minute+5),
                        associatedAxisStart: widget.lowBegin,
                        associatedAxisEnd: widget.lowEnd,
                        color: widget.title=="SpO2"?Colors.red:blueLow
                    ),
                    PlotBand(
                        isVisible: true,
                        start: chartData[0].x,
                        end: DateTime(chartData[0].x.year, chartData[0].x.month, chartData[0].x.day, chartData[0].x.hour, chartData[0].x.minute+5),
                        associatedAxisStart: widget.normalBegin,
                        associatedAxisEnd: widget.normalEnd,
                        color: Colors.green
                    ),
                    PlotBand(
                        isVisible: true,
                        start: chartData[0].x,
                        end: DateTime(chartData[0].x.year, chartData[0].x.month, chartData[0].x.day, chartData[0].x.hour, chartData[0].x.minute+5),
                        associatedAxisStart: widget.highBegin,
                        associatedAxisEnd: widget.highEnd,
                        color: Colors.red
                    ),

                  ],
                  majorGridLines: MajorGridLines(width: 0),
                ),
                primaryYAxis: NumericAxis(

                    interval: 1,
                    maximum:widget.unit=="%"?100: getMaximumVital(chartData) + 20,
                    minimum:widget.unit=="%" && getMinVital(chartData)-10 <0 ?0 :widget.unit=="%"?getMinVital(chartData)-10: getMinVital(chartData) - 20,
                    axisLine: AxisLine(
                      color: primary,
                    )
                ),
                series: <LineSeries<ChartData, DateTime>>[
                  // Renders column chart
                  LineSeries<ChartData, DateTime>(
                      dataSource: chartData,
                      pointColorMapper:(ChartData data, _) => data.y>widget.highBegin?Colors.red:data.y>widget.lowEnd?Colors.green:widget.title=="SpO2"?Colors.red:blueLow,
                      xValueMapper: (ChartData data, _) => data.x,
                      yValueMapper: (ChartData data, _) => data.y,

                      dataLabelMapper: (ChartData data, _) => data.y.toString(),

                      dataLabelSettings: DataLabelSettings(
                          isVisible: true,
                          // Templating the data label
                          builder: (dynamic data, dynamic point, dynamic series, int pointIndex, int seriesIndex) {
                            ChartData vital = data;

                            if(highList.contains(vital)) {

                              log(vital.x.toString());
                              return Container(
                                  height: 40,
                                  width: 40,
                                  child: Image.asset(widget.highAlertImage)
                              );
                            }
                            else if (lowList.contains(vital)){

                              return Container(
                                  height: 40,
                                  width: 40,
                                  child: Image.asset(widget.lowAlertImage)
                              );
                            }
                            else{
                              return Container(

                              );
                            }
                          }

                      )
                  )
                ],
                tooltipBehavior: _tooltipBehavior,
                onTooltipRender: (TooltipArgs args) {
                  if (args.pointIndex != null) {

                    args.text=args.dataPoints![args.pointIndex!.toInt()].x.hour>12? "${args.dataPoints![args.pointIndex!.toInt()].x.hour-12}:00 PM":
                    "${args.dataPoints![args.pointIndex!.toInt()].x.hour}:00 AM";
                    String temp = args.dataPoints![args.pointIndex!.toInt()].x.hour+1>12? "${args.dataPoints![args.pointIndex!.toInt()].x.hour+1-12}:00 PM":
                    "${args.dataPoints![args.pointIndex!.toInt()].x.hour+1}:00 AM";
                    args.text = args.text! + " to $temp";
                    // check the value of y axis, if more than high then show red color image
                    if(args.dataPoints![args.pointIndex!.toInt()].y>widget.highBegin){
                      args.header = "High Rate - ${args.dataPoints![args.pointIndex!.toInt()].y}${widget.unit}";

                    }
                    else if(args.dataPoints![args.pointIndex!.toInt()].y<widget.lowEnd){
                      args.header = "Low Rate - ${args.dataPoints![args.pointIndex!.toInt()].y}${widget.unit}";
                    }
                    else{
                      args.header = "Normal Rate - ${args.dataPoints![args.pointIndex!.toInt()].y}${widget.unit}";
                    }
                  }
                },
                zoomPanBehavior: _zoomPanBehavior,
              )
          ),
        ],
      ),

    );
  }
  ChartData getMedianHighVital(List<ChartData> data) {
    List<ChartData> vital = [];
    for(int i=0;i<data.length;i++){
      if (data[i].y>widget.highBegin)
        vital.add(data[i]);
    }
    // sort the list w.r.t y
    vital.sort((a,b)=>a.y.compareTo(b.y));
    return vital.length>0?vital[vital.length~/2]:ChartData(DateTime.now(),-1);
  }

  ChartData getMedianLowVital(List<ChartData> data) {

    List<ChartData> vital = [];
    for(int i=0;i<data.length;i++){
      if (data[i].y<widget.lowEnd)
        vital.add(data[i]);
    }
    vital.sort((a,b)=>a.y.compareTo(b.y));
    return vital.length>0?vital[vital.length~/2]:ChartData(DateTime.now(),-1);
  }

  /// makes list of list of vital values such that each list contains a set of temperatures where they are in low range, normal range and high range and continous in time
  List<List<ChartData>> partitionVitalValues(List<ChartData> data){
    List<List<ChartData>> vital = [];
    int i = 0;
    while(i<data.length){
      if(i<data.length&&data[i].y > widget.highBegin){
        List<ChartData> high = [];
        while(i<data.length && data[i].y>widget.highBegin){
          high.add(data[i]);
          i++;
        }
        vital.add(high);

      }else if(i<data.length&& data[i].y<widget.lowEnd){
        List<ChartData> low = [];
        while(i<data.length && data[i].y<widget.lowEnd){
          low.add(data[i]);
          i++;
        }
        vital.add(low);
      }
      else{
        i++;
      }

    }
    return vital;

  }
}

double getMaximumVital(List<ChartData> data){
  double max = double.negativeInfinity;
  for(int i=0;i<data.length;i++){
    if(data[i].y>max){
      max = data[i].y;
    }
  }
  return max.ceilToDouble();
}
double getMinVital(List<ChartData> data){
  double min = double.infinity;
  for(int i=0;i<data.length;i++){
    if(data[i].y<min){
      min = data[i].y;
    }
  }
  return min.floorToDouble();
}

