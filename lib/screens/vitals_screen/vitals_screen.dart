import 'package:flutter/material.dart';
import 'package:neoroo/screens/heart_rate_screen/heart_rate_screen.dart';
import 'package:neoroo/screens/notifications_screen/notifications_screen.dart';
import 'package:neoroo/screens/oxygen_rate_screen/oxygen_rate_screen.dart';
import 'package:neoroo/screens/raspiration_screen/raspiration_screen.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/screens/temperature_screen/temperature_screen.dart';
import 'package:neoroo/screens/vitals_screen/components/vitals_card.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';
import 'package:neoroo/routing/routing.dart' as router;
class VitalsScreen extends StatefulWidget {
  const VitalsScreen({Key? key, required this.temperatureSeverity, required this.heartRateSeverity, required this.respirationSeverity, required this.oxygenSaturationSeverity}) : super(key: key);
  final VitalSeverity temperatureSeverity;
  final VitalSeverity heartRateSeverity;
  final VitalSeverity respirationSeverity;
  final VitalSeverity oxygenSaturationSeverity;
  @override
  State<VitalsScreen> createState() => _VitalsScreenState();
}

class _VitalsScreenState extends State<VitalsScreen> {
  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight= MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: ScreenHeader(
        isUserIconVisible: false,
        title: "Vitals",
        hasBackButton: true,
        actionIconImages: [
          "assets/images/notification.png",
        ],
        actionIconOnPressFunctions: [
          () {
            pushScreen(context, screen: NotificationsScreen(), settings: RouteSettings(name: router.notificationsPage));

          },
        ],
        backButtonOnPressed:(){
          Navigator.pop(context);
        }

      ),
      backgroundColor: secondaryBackground,
      body: Container(

            child: (deviceWidth > deviceHeight && deviceWidth > 600)
                || deviceWidth > 960 ?
            SingleChildScrollView(
                child:Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: convertXdWidthToScreen(xdWidth: 25, deviceWidth: deviceWidth, xdScreenWidth: 360),
                        vertical: convertXdHeightToScreen(xdHeight: 70, deviceHeight: deviceHeight, xdScreenHeight: 640),
                      ),
                      child:SizedBox(
                        width: deviceWidth * 0.4,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            buildTemperatureVitalCard(context),
                            SizedBox(height: convertXdWidthToScreen(xdWidth: 16, deviceWidth: deviceWidth, xdScreenWidth: 360),),
                            buildRespirationVitalCard(context),
                            SizedBox(height: convertXdWidthToScreen(xdWidth: 16, deviceWidth: deviceWidth, xdScreenWidth: 360),),
                            buildHeartVitalCard(context),
                            SizedBox(height: convertXdWidthToScreen(xdWidth: 16, deviceWidth: deviceWidth, xdScreenWidth: 360),),
                            buildSpO2VitalCard(context),
                            SizedBox(height: convertXdWidthToScreen(xdWidth: 16, deviceWidth: deviceWidth, xdScreenWidth: 360),),
                          ],
                ),
              ),))
            ):
            Center(
              child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: convertXdWidthToScreen(xdWidth: 25, deviceWidth: deviceWidth, xdScreenWidth: 360),
              vertical: convertXdHeightToScreen(xdHeight: 70, deviceHeight: deviceHeight, xdScreenHeight: 640),
            ),
        child:Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Row(
                  children: [
                    Expanded(
                      child: buildTemperatureVitalCard(context),
                    ),
                    SizedBox(width: convertXdWidthToScreen(xdWidth: 14, deviceWidth: deviceWidth, xdScreenWidth: 360),),
                    Expanded(
                      child: buildRespirationVitalCard(context),
                    )
                  ],
                  ),
                ),
                SizedBox(height: convertXdWidthToScreen(xdWidth: 16, deviceWidth: deviceWidth, xdScreenWidth: 360),),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: buildHeartVitalCard(context),
                      ),
                      SizedBox(width: convertXdWidthToScreen(xdWidth: 14, deviceWidth: deviceWidth, xdScreenWidth: 360),),
                      Expanded(
                        child: buildSpO2VitalCard(context),
                      )
                    ],
                  ),
                )

              ],
            ),
          )
        ),
      ),
    );
  }

  VitalTypeCard buildSpO2VitalCard(BuildContext context) {
    return VitalTypeCard(
                          title: "Blood Oxygen Saturation",
                        primaryIcon: widget.oxygenSaturationSeverity == VitalSeverity.normal ? "assets/images/oxygen-normal.png": widget.oxygenSaturationSeverity == VitalSeverity.high ? "assets/images/oxygen-high.png" : "assets/images/oxygen-low.png",
                          onTap: (){
                            pushScreen(context, screen: SpO2RateScreen(), settings: RouteSettings(name: router.spo2Page));
                          }
                      );
  }

  VitalTypeCard buildHeartVitalCard(BuildContext context) {
    return VitalTypeCard(
                        title: "Heart Rate",
                        primaryIcon: widget.heartRateSeverity == VitalSeverity.normal ? "assets/images/pulse-normal.png": widget.heartRateSeverity == VitalSeverity.high ? "assets/images/pulse-high.png" : "assets/images/pulse-low.png",
                        onTap: (){
                          pushScreen(context, screen: HeartRateScreen(), settings: RouteSettings(name: router.heartRatePage));
                        },
                      );
  }

  VitalTypeCard buildRespirationVitalCard(BuildContext context) {
    return VitalTypeCard(
                      title: "Respiration Rate",
                      primaryIcon: widget.respirationSeverity== VitalSeverity.normal ? "assets/images/lungs-normal.png": widget.respirationSeverity == VitalSeverity.high ? "assets/images/lungs-high.png" : "assets/images/lungs-low.png",
                      onTap: (){
                        pushScreen(context, screen: RespirationRateScreen(), settings: RouteSettings(name: router.respirationPage));
                      }
                    );
  }

  VitalTypeCard buildTemperatureVitalCard(BuildContext context) {
    return VitalTypeCard(
                      title: "Temperature",
                      primaryIcon: widget.temperatureSeverity == VitalSeverity.normal ? "assets/images/normal-temperature.png": widget.temperatureSeverity == VitalSeverity.high ? "assets/images/high-temperature.png" : "assets/images/low-temperature.png",
                      onTap: (){
                        pushScreen(context, screen: TemperatureScreen(), settings: RouteSettings(name: router.temperaturePage));
                      },
                    );
  }
}


