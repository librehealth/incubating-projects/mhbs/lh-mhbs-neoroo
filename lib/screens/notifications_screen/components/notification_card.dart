import 'package:flutter/material.dart';

class NotificationCard extends StatelessWidget {
  const NotificationCard({
    super.key,
    this.isRead = false,
    required this.title,
    required this.description,
    required this.date,
    required this.onTap,
  });
  final bool isRead;
  final String title;
  final String description;
  final String date;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: InkWell(
        onTap: (){
          onTap.call();
        },
        child: Ink(
          color:  Color(0x66F6F6F6),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: TextStyle(
                          color: Colors.black.withOpacity(isRead ? 0.4 : 1),
                          fontSize: 16,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.w500,
                          height: 1.14,
                        ),
                      ),
                      SizedBox(height: 8,),
                      Text(
                        description,
                        style: TextStyle(
                          color: Colors.black.withOpacity(isRead ? 0.4 : 1),
                          fontSize: 14,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                ),
                Text(
                  date,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    color: Color(0xFF0078D4),
                    fontSize: 14,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w400,

                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}