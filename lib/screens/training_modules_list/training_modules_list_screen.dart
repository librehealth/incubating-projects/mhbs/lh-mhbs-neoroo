import 'package:flutter/material.dart';
import 'package:neoroo/screens/notifications_screen/components/notification_card.dart';
import 'package:neoroo/screens/notifications_screen/notifications_screen.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/screens/training_module_screen/training_module_screen.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';
import 'package:neoroo/routing/routing.dart' as router;
class TrainingModulesList extends StatefulWidget {
  const TrainingModulesList({Key? key}) : super(key: key);

  @override
  State<TrainingModulesList> createState() => _TrainingModulesListState();
}

class _TrainingModulesListState extends State<TrainingModulesList> {
  late TextEditingController searchController;
  @override
  void initState() {
    // TODO: implement initState
    searchController = TextEditingController();
    super.initState();
  }
  List<Map<String, dynamic>> modulesList = [
    {
      "title": "Troubleshoot Neowarm",
      "description":
      "Dr. Rio teaches how to do basic troubleshoot when neowarm device malfunctions.",
      "date": '6 JUN ‘20',
      "isRead": false,
    },
    {
      "title": "NeoRoo app walkthrough",
      "description":
      "Dr. Nia teaches how to install NeoRoo app and get started with the features.",
      "date": '6 JUN ‘20',
      "isRead": false,
    },
    {
      "title": "Parenting Tips",
      "description":
      "Dr. Rio teaches how to take care of newborns until discharge date.",
      "date": 'Completed',
      "isRead": true,
    },
    {
      "title": "Kangaroo Care is not just for Moms!",
      "description":
      "Any adult can kangaroo with a baby. Listen to the step-by-step guide.",
      "date": 'Completed',
      "isRead": true,
    },
    {
      "title": "Exclusive breastfeeding",
      "description":
      "Nurse Angelique provide tips & strategies for successful breastfeeding.",
      "date": 'Completed',
      "isRead": true,
    },

  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ScreenHeader(
        isUserIconVisible: false,
        title: "Training Modules",
        actionIconImages: [
          "assets/images/notification.png",
        ],
        actionIconOnPressFunctions: [
              () {
                pushScreen(context, screen: NotificationsScreen(), settings: RouteSettings(name: router.notificationsPage));

              },
        ],
        hasMenuButton: true,
        menuItems: [
          "Latest",
          "Popular",
          "Recently Viewed",
          "Bookmarked"
        ],
        menuIcons: [
          "assets/images/new.png",
          "assets/images/recently.png",
          "assets/images/bookmarked.png",
          "assets/images/popular.png"
        ],
        menuItemOnPressFunctions: [
          () {},
          () {},
          (){},
          (){}
        ],
        hasBackButton: true,
        backButtonOnPressed: () {
          Navigator.pop(context);
        },
        hasSearchBar: true,
        searchController: searchController,
        searchHintText: "Search",
        onSearchTextChanged: (text) {
          setState(() {
            // search text
          });
        },
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
              children:
              modulesList.map((e) =>
                  NotificationCard(
                    title: e['title'],
                    description:
                    e['description'],
                    date: e['date'] ,
                    isRead: e['isRead'] ,
                    onTap: () {
                      pushScreen(context, screen: TrainingModuleScreen(), settings: RouteSettings(name: router.trainingModulePage));
                    },

                  )
              ).toList()

          ),
        ),
      ),
    );
  }
}
