import 'package:audioplayers/audioplayers.dart' as audio;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:neoroo/screens/notifications_screen/notifications_screen.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:neoroo/utils/vertical_space.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:neoroo/routing/routing.dart' as router;
class TrainingModuleScreen extends StatefulWidget {
  const TrainingModuleScreen({Key? key}) : super(key: key);

  @override
  State<TrainingModuleScreen> createState() => _TrainingModuleScreenState();
}

class _TrainingModuleScreenState extends State<TrainingModuleScreen> {
  late TextEditingController searchController;
  late YoutubePlayerController _youtubeController;

  bool isBookmark = false;
  late PlayerState _playerState;
  late YoutubeMetaData _videoMetaData;
  double _volume = 100;
  bool _muted = false;
  int maxduration = 100;
  int currentpos = 0;
  String currentpostlabel = "00:00";
  bool isplaying = false;
  bool audioplayed = false;
  String audioUrl = "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-3.mp3";
  String trainingModuleYoutubeUrl = "https://www.youtube.com/watch?v=OkhSJ16FHfY";

  bool _isPlayerReady = false;
  late audio.AudioPlayer player;
  void listener() {
    if (_isPlayerReady && mounted && !_youtubeController.value.isFullScreen) {
      setState(() {
        _playerState = _youtubeController.value.playerState;
        _videoMetaData = _youtubeController.metadata;
      });
    }
  }
  @override
  void initState() {
    player = audio.AudioPlayer();

    _youtubeController = YoutubePlayerController(
        initialVideoId: YoutubePlayer.convertUrlToId(trainingModuleYoutubeUrl)!,
        flags: YoutubePlayerFlags(
          autoPlay: false,
          mute: false,
          loop: true
        ))..addListener(listener);

    _videoMetaData = const YoutubeMetaData();
    _playerState = PlayerState.unknown;
    // get the duration of a url audio
    player.onDurationChanged.listen((Duration d) {
      setState(() {
        maxduration = d.inMilliseconds;
      });
    });

    searchController = TextEditingController();
    super.initState();


  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _youtubeController.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    player.dispose();
    _youtubeController.dispose();
    searchController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: ScreenHeader(
        isUserIconVisible: false,
        title: "Training Modules",
        actionIconImages: [
          "assets/images/notification.png",
        ],
        actionIconOnPressFunctions: [
              () {
                pushScreen(context, screen: NotificationsScreen(), settings: RouteSettings(name: router.notificationsPage));

          },
        ],
        hasMenuButton: true,
        menuItems: [
          "Latest",
          "Popular",
          "Recently Viewed",
          "Bookmarked"
        ],
        menuIcons: [
          "assets/images/new.png",
          "assets/images/recently.png",
          "assets/images/bookmarked.png",
          "assets/images/popular.png"
        ],
        menuItemOnPressFunctions: [
              () {},
              () {},
              (){},
              (){}
        ],
        hasBackButton: true,
        backButtonOnPressed: () {
          Navigator.pop(context);
        },
        hasSearchBar: true,
        searchController: searchController,
        searchHintText: "Search",
        onSearchTextChanged: (text) {
          setState(() {
            // search text
          });
        },
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left:
          convertXdWidthToScreen(xdWidth: 30, deviceWidth: deviceWidth, xdScreenWidth: 360),
            right: convertXdWidthToScreen(xdWidth: 20, deviceWidth: deviceWidth, xdScreenWidth: 360),
            top: convertXdHeightToScreen(xdHeight: 20, deviceHeight: deviceHeight, xdScreenHeight: 640),
            bottom: convertXdHeightToScreen(xdHeight: 20, deviceHeight: deviceHeight, xdScreenHeight: 640),
          ),
          child: Column(
              children:[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: Text("How to wear NeoWarm Device ?",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),)),
                    IconButton(onPressed: (){
                      setState(() {
                        isBookmark = !isBookmark;
                      });
                    },icon: Icon(isBookmark?FontAwesomeIcons.solidBookmark:
                      FontAwesomeIcons.bookmark, color: primary, size: (deviceWidth > deviceHeight && deviceWidth > 600)
                        || deviceWidth >960? convertXdHeightToScreen(xdHeight: 16, deviceHeight: deviceWidth, xdScreenHeight: 640):
                    convertXdWidthToScreen(xdWidth: 16, deviceWidth: deviceWidth, xdScreenWidth: 360),))
                  ],
                ),
                Padding(
                  padding:  EdgeInsets.only(
                    right: convertXdWidthToScreen(xdWidth: 10, deviceWidth: deviceWidth, xdScreenWidth: 360),

                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      VerticalSpace(height: 20,),
                      Center(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child:Container(
                            height: (deviceWidth > deviceHeight && deviceWidth > 600)
                              || deviceWidth >960?deviceWidth*0.3:convertXdHeightToScreen(xdHeight: 138, deviceHeight: deviceHeight, xdScreenHeight: 640),
                            width: (deviceWidth > deviceHeight && deviceWidth > 600)
                                || deviceWidth >960? deviceWidth*0.4: null,
                            color: primary,
                            child:  YoutubePlayer(
                              controller: _youtubeController,
                              onReady: () {
                                setState(() {
                                  _isPlayerReady = true;

                                });
                              },
                              onEnded: (data) {

                              },
                              showVideoProgressIndicator: true,
                              progressIndicatorColor: primary,
                              progressColors: ProgressBarColors(
                                playedColor: primary,
                                handleColor: primary,
                              ),

                            ),
                          )
                        ),
                      ),
                      VerticalSpace(height: 20,),
                      Text("Cup Feeding Small Babies",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                      VerticalSpace(height: 10,),
                      Text("If a small baby is able to swallow but not yet able to feed well enough from the breast, the mother can feed her baby by cup. This video shows how to feed a baby safely using a small cup, as well as a spoon, a paladai, and a Nifty cup."
                        ,style: TextStyle(fontSize: 16,color: Colors.black54),),
                      VerticalSpace(height: 20,),
                      Text("Listen to audio",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                      VerticalSpace(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Color(0xFFE5E5E5),
                              ),
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                        onTap: () async {
                                          if(!isplaying && !audioplayed){
                                            int result = await player.play(
                                              audio.UrlSource(
                                                  audioUrl
                                              )
                                            ).then((value) => 1);
                                            if(result == 1){ //play success
                                              setState(() {

                                                player.onDurationChanged.listen((Duration d){
                                                  setState(() {
                                                    maxduration = d.inMilliseconds; //get the total duration of playing audio
                                                  });
                                                });
                                                player.onPositionChanged.listen((Duration  p){
                                                  currentpos = p.inMilliseconds; //get the current position of playing audio
                                                  setState(() {
                                                    //refresh the UI
                                                  });
                                                });
                                                isplaying = true;
                                                audioplayed = true;
                                              });
                                            }else{
                                              print("Error while playing audio.");
                                            }
                                          }else if(audioplayed && !isplaying){
                                            int result = await player.resume().then((value) => 1);
                                            if(result == 1){ //resume success
                                              setState(() {
                                                isplaying = true;
                                                audioplayed = true;
                                              });
                                            }else{
                                              print("Error on resume audio.");
                                            }
                                          }else{
                                            int result = await player.pause().then((value) => 1);
                                            if(result == 1){ //pause success
                                              setState(() {
                                                isplaying = false;
                                              });
                                            }else{
                                              print("Error on pause audio.");
                                            }
                                          }
                                          },
                                        child: CircleAvatar(
                                          radius: 12,
                                            child: Icon(isplaying?Icons.pause:Icons.play_arrow,color: Colors.white, size: 18,),
                                            backgroundColor: primary,
                                        ),
                                      ),
                                      Expanded(child: Slider(

                                        value: double.parse(currentpos.toString()),
                                        min: 0,
                                        max: double.parse(maxduration.toString()),
                                        divisions: maxduration,
                                        activeColor: primary,
                                        inactiveColor: primary.withOpacity(0.2),
                                        onChanged: (double value) async {
                                          int seekval = value.round();
                                          int result = await player.seek(Duration(milliseconds: seekval)).then((value) => 1);
                                          if(result == 1){ //seek successful
                                            currentpos = seekval;
                                          }else{
                                            print("Seek unsuccessful.");
                                          }
                                        },
                                      ),),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: GestureDetector(
                              onTap: (){
                                if(_muted){
                                  player.setVolume(1);
                                }else{
                                  player.setVolume(0);
                                }
                                setState(() {
                                  _muted = !_muted;
                                });
                              },
                              child: Icon(
                                _muted?FontAwesomeIcons.volumeOff: FontAwesomeIcons.volumeHigh,
                                color: primary,
                                size: 18,
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),

              ]
          ),
        ),
      ),
    );
  }
}
