import 'package:flutter/material.dart';
import 'package:neoroo/utils/enums.dart';

class HighPriorityAlertCard extends StatelessWidget {
  const HighPriorityAlertCard({Key? key, required this.content, required this.severity, required this.type}) : super(key: key);
  final String content;
  final VitalType type;
  final VitalSeverityType severity;
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color:severity==VitalSeverityType.high? Colors.red:Colors.blue,
                    borderRadius: BorderRadius.only(topRight: Radius.circular(4))
                  ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 3.0),
                  child: Text(severity==VitalSeverityType.high? "HIGH":"LOW",
                  style: TextStyle(
                    color: Colors.white,

                    fontSize: 8,
                    fontWeight: FontWeight.w600
                  ),
                  ),
                ),
                )
              ],

            ),
            const SizedBox(height: 2.5,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  type==VitalType.respiratoryRate? Image.asset("assets/images/lungs-main.png", width: 31,):
                      type==VitalType.heartRate? Image.asset("assets/images/heart-beat-main.png", width: 31,):
                      type==VitalType.bloodOxygen? Image.asset("assets/images/blood-oxygen-main.png", width: 31,):
                           Image.asset("assets/images/thermometer-main.png", width: 31,),
                  SizedBox(width: 16,),
                  Flexible(
                    child: Text(content,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400
                    ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      );
  }
}
