import 'package:flutter/material.dart';
import 'package:neoroo/utils/constants.dart';

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

const weekdays=[
  "S",
  "M",
  "T",
  "W",
  "T",
  "F",
  "S"

];

class WeeklyCalendar extends StatefulWidget {
  const WeeklyCalendar({Key? key, required this.finalDate, required this.onDateChange}) : super(key: key);
  final DateTime finalDate;
  final VoidCallback onDateChange;
  @override
  State<WeeklyCalendar> createState() => _WeeklyCalendarState();
}

class _WeeklyCalendarState extends State<WeeklyCalendar> {


  int selected= DateTime.now().day;
  @override
  void initState() {

    selected = DateTime.now().day;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight= MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("${months[DateTime.now().month - 1]} ${DateTime.now().year}",
            style: TextStyle(
                fontSize: (deviceWidth > deviceHeight && deviceWidth > 600)
                    || deviceWidth >960?20:16,
                fontWeight: FontWeight.w400,
                color: Colors.black
            ),
          ),
          const SizedBox(height: 16,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children:
            weekdays.map((e) => Container(
              width:(deviceWidth > deviceHeight && deviceWidth > 600)
                  || deviceWidth >960?(deviceWidth-32)/18: (deviceWidth-32)/9,
              child: Center(
                child: Text(e,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Colors.black
                  ),
                ),
              ),
            ),).toList(),

          ),
          const SizedBox(height: 8,),
          const Divider(
            color: Color(0xffE5E5E5),
            thickness: 1,
          ),
          const SizedBox(height: 8,),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [for(var i = 0; i < 7; i++)...[
                GestureDetector(
                  onTap: (){

                    setState(() {
                      selected = widget.finalDate.add(Duration(days: i)).day;
                    });
                    widget.onDateChange.call();
                  },
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 300),

                    decoration: BoxDecoration(
                      color: selected == widget.finalDate.add(Duration(days: i)).day ? primary : i == DateTime.now().weekday?primary.withOpacity(0.2): Colors.white,
                      borderRadius: BorderRadius.circular(100),

                    ),
                    width:(deviceWidth > deviceHeight && deviceWidth > 600)
                        || deviceWidth >960?(deviceWidth-32)/18: (deviceWidth-32)/9,
                    height:(deviceWidth > deviceHeight && deviceWidth > 600)
                        || deviceWidth >960?(deviceWidth-32)/18: (deviceWidth-32)/9,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text("${widget.finalDate.add(Duration(days: i)).day}",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: selected == widget.finalDate.add(Duration(days: i)).day ? Colors.white:Colors.black
                          ),
                        ),
                      ),
                    ),
                  ),
                ),],]
          ),
        ],
      ),
    );
  }
}