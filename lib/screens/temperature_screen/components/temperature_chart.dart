import 'dart:developer';

import 'package:el_tooltip/el_tooltip.dart';
import 'package:flutter/material.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:syncfusion_flutter_charts/charts.dart';


class TemperatureChart extends StatefulWidget {
  const TemperatureChart({Key? key, required this.chartData, required this.unit}) : super(key: key);
  /// Important! The chartData must be sorted by given by celsius
  final List<ChartData> chartData;
  final Unit unit;
  @override
  State<TemperatureChart> createState() => _TemperatureChartState();

}

class _TemperatureChartState extends State<TemperatureChart> {
  late TooltipBehavior  _tooltipBehavior;
  late ZoomPanBehavior _zoomPanBehavior;
  List<List<ChartData>> chartDataList = [];
  List<ChartData> lowList = [];
  List<ChartData> highList = [];
  @override
  void initState() {
    // TODO: implement initState
    _tooltipBehavior = TooltipBehavior(enable: true,

    );
    _zoomPanBehavior = ZoomPanBehavior(
      enablePinching: true,
      zoomMode: ZoomMode.x,
      enablePanning: true,
      enableDoubleTapZooming:  true,
    );
    widget.chartData.sort((a,b)=>a.x.compareTo(b.x));
    chartDataList = partitionTemperatures(widget.chartData);
    for (var i = 0; i < chartDataList.length; i++) {
      if(chartDataList[i][0].y>37.5){
        highList.add(getMedianHighTemp(chartDataList[i]));
      }
      else if(chartDataList[i][0].y<35.5){
        lowList.add(getMedianLowTemp(chartDataList[i]));
      }
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final List<ChartData> chartData = widget.chartData;

    chartDataList = partitionTemperatures(widget.chartData);
    for (var i = 0; i < chartDataList.length; i++) {
      if(chartDataList[i][0].y>37.5){
        highList.add(getMedianHighTemp(chartDataList[i]));
      }
      else if(chartDataList[i][0].y<35.5){
        lowList.add(getMedianLowTemp(chartDataList[i]));
      }
    }
    return  Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Temperature",
              style: TextStyle(
                color: primary,
              ),
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width*0.9,
                height: MediaQuery.of(context).size.height*0.4,
                child: SfCartesianChart(
                  plotAreaBorderWidth: 0,
                  primaryXAxis: DateTimeAxis(
                    labelPosition: ChartDataLabelPosition.outside,
                    title: AxisTitle(text: 'Time',
                        textStyle: TextStyle(
                            color: primary
                        ),
                        alignment: ChartAlignment.far),
                    axisLine: AxisLine(color: primary),
                    interval: 1,
                    plotBands: <PlotBand>[
                      PlotBand(
                          isVisible: true,
                          start: chartData[0].x,
                          end: DateTime(chartData[0].x.year, chartData[0].x.month, chartData[0].x.day, chartData[0].x.hour, chartData[0].x.minute+5),
                          associatedAxisStart: widget.unit==Unit.Fahrenheit?convertCelsiusToFahrenheit(celsius:30):30,
                          associatedAxisEnd: widget.unit==Unit.Fahrenheit?convertCelsiusToFahrenheit(celsius:35.5):35.5,
                          color: blueLow
                      ),
                      PlotBand(
                          isVisible: true,
                          start: chartData[0].x,
                          end: DateTime(chartData[0].x.year, chartData[0].x.month, chartData[0].x.day, chartData[0].x.hour, chartData[0].x.minute+5),
                          associatedAxisStart:widget.unit==Unit.Fahrenheit?convertCelsiusToFahrenheit(celsius:35.5 ): 35.5,
                          associatedAxisEnd: widget.unit==Unit.Fahrenheit?convertCelsiusToFahrenheit(celsius:37.5):37.5,
                          color: Colors.green
                      ),
                      PlotBand(
                          isVisible: true,
                          start: chartData[0].x,
                          end: DateTime(chartData[0].x.year, chartData[0].x.month, chartData[0].x.day, chartData[0].x.hour, chartData[0].x.minute+5),
                          associatedAxisStart: widget.unit==Unit.Fahrenheit?convertCelsiusToFahrenheit(celsius:37.5):37.5,
                          associatedAxisEnd: widget.unit==Unit.Fahrenheit?convertCelsiusToFahrenheit(celsius:40):40,
                          color: Colors.red
                      ),

                    ],
                    majorGridLines: MajorGridLines(width: 0),
                  ),
                  primaryYAxis: NumericAxis(
                      interval: 1,
                      maximum: widget.unit==Unit.Fahrenheit?convertCelsiusToFahrenheit(celsius: getMaximumTemp(chartData) + 1): getMaximumTemp(chartData)+1,
                      minimum: widget.unit==Unit.Fahrenheit?convertCelsiusToFahrenheit(celsius: getMinTemp(chartData) - 1):getMinTemp(chartData)-1,
                      axisLine: AxisLine(
                        color: primary,

                      )
                  ),
                  series: <LineSeries<ChartData, DateTime>>[
                    // Renders column chart
                    LineSeries<ChartData, DateTime>(
                        dataSource: chartData,
                        pointColorMapper:(ChartData data, _) => data.y>37.5?Colors.red:data.y>35.5?Colors.green:blueLow,
                        xValueMapper: (ChartData data, _) => data.x,
                        yValueMapper: (ChartData data, _) => widget.unit==Unit.Fahrenheit?convertCelsiusToFahrenheit(celsius:data.y):data.y,

                        dataLabelMapper: (ChartData data, _) => widget.unit==Unit.Fahrenheit?convertCelsiusToFahrenheit(celsius:data.y).toString():data.y.toString(),

                        dataLabelSettings: DataLabelSettings(
                            isVisible: true,
                            // Templating the data label
                            builder: (dynamic data, dynamic point, dynamic series, int pointIndex, int seriesIndex) {
                              ChartData temp = data;

                              if(highList.contains(temp)) {
                                log("high");
                                log(temp.x.toString());
                                return Container(
                                    height: 40,
                                    width: 40,
                                    child: Image.asset('assets/images/high-temperature-red.png')
                                );
                              }
                              else if (lowList.contains(temp)){
                                log("low");
                                return Container(
                                    height: 40,
                                    width: 40,
                                    child: Image.asset('assets/images/low-temperature-blue.png')
                                );
                              }
                              else{
                                return Container(

                                );
                              }
                            }

                        )
                    )
                  ],
                  tooltipBehavior: _tooltipBehavior,
                  onTooltipRender: (TooltipArgs args) {
                    if (args.pointIndex != null) {

                      args.text=args.dataPoints![args.pointIndex!.toInt()].x.hour>12? "${ args.dataPoints![args.pointIndex!.toInt()].x.hour-12}:00 PM":
                      "${args.dataPoints![args.pointIndex!.toInt()].x.hour}:00 AM";
                      String temp = args.dataPoints![args.pointIndex!.toInt()].x.hour+1>12? "${args.dataPoints![args.pointIndex!.toInt()].x.hour+1 -12}:00 PM":
                      "${args.dataPoints![args.pointIndex!.toInt()].x.hour+1}:00 AM";
                      args.text = args.text! + " to $temp";
                      // check the value of y axis, if more than 37.5 then show red color image
                      if(args.dataPoints![args.pointIndex!.toInt()].y>37.5){
                        args.header = "High Temperature - ${args.dataPoints![args.pointIndex!.toInt()].y}${widget.unit==Unit.Fahrenheit?"F":"°"}";

                      }
                      else if(args.dataPoints![args.pointIndex!.toInt()].y<35.5){
                        args.header = "Low Temperature - ${args.dataPoints![args.pointIndex!.toInt()].y}${widget.unit==Unit.Fahrenheit?"F":"°"}";
                      }
                      else{
                        args.header = "Normal Temperature - ${args.dataPoints![args.pointIndex!.toInt()].y}${widget.unit==Unit.Fahrenheit?"F":"°"}";
                      }
                    }
                  },
                  zoomPanBehavior: _zoomPanBehavior,
                )
            ),
          ],
        ),

    );
  }
}
class ChartData{
  ChartData(this.x, this.y);
  final DateTime x;
  final double y;
}
double getMaximumTemp(List<ChartData> data){
  double max = 0;
  for(int i=0;i<data.length;i++){
    if(data[i].y>max){
      max = data[i].y;
    }
  }
  return max.ceilToDouble();
}
double getMinTemp(List<ChartData> data){
  double min = 100;
  for(int i=0;i<data.length;i++){
    if(data[i].y<min){
      min = data[i].y;
    }
  }
  return min.floorToDouble();
}

ChartData getMedianHighTemp(List<ChartData> data) {
  List<ChartData> temp = [];
  for(int i=0;i<data.length;i++){
    if (data[i].y>37.5)
      temp.add(data[i]);
  }
  // sort the list w.r.t y
  temp.sort((a,b)=>a.y.compareTo(b.y));
  return temp.length>0?temp[temp.length~/2]:ChartData(DateTime.now(),-1);
}

ChartData getMedianLowTemp(List<ChartData> data) {

  List<ChartData> temp = [];
  for(int i=0;i<data.length;i++){
    if (data[i].y<35.5)
      temp.add(data[i]);
  }
  temp.sort((a,b)=>a.y.compareTo(b.y));
  return temp.length>0?temp[temp.length~/2]:ChartData(DateTime.now(),-1);
}

/// makes list of list of temperatures such that each list contains a set of temperatures where they are in low range, normal range and high range and continous in time
List<List<ChartData>> partitionTemperatures(List<ChartData> data){
  List<List<ChartData>> temp = [];
  int i = 0;
  while(i<data.length){
    if(i<data.length&&data[i].y > 37.5){
      List<ChartData> high = [];
      while(i<data.length && data[i].y>37.5){
        high.add(data[i]);
        i++;
      }
      temp.add(high);

    }else if(i<data.length&& data[i].y<35.5){
      List<ChartData> low = [];
      while(i<data.length && data[i].y<35.5){
        low.add(data[i]);
        i++;
      }
      temp.add(low);
    }
    else{
      i++;
    }

  }
  return temp;

}