import 'package:flutter/material.dart';
import 'package:neoroo/utils/constants.dart';

class SignUpWidget extends StatelessWidget {
  const SignUpWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return RichText(
        text: TextSpan(
            text: "Don't have an account ? ",
            style: TextStyle(
                fontFamily: lato,
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color: Colors.black),
            children: <TextSpan>[
          TextSpan(
            text: "Sign Up",
            style: TextStyle(
                fontFamily: lato,
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color: Colors.purple),
          )
        ]));
  }
}
