import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:neoroo/screens/add_baby/add_baby.dart';
import 'package:neoroo/screens/home/home.dart';

import 'package:neoroo/screens/messaging/messaging_home.dart';
import 'package:neoroo/screens/more_options/more_options.dart';
import 'package:neoroo/screens/scan_qr_code/scan_qr_code.dart';
import 'package:neoroo/screens/skin_to_skin_time/skin_to_skin_time.dart';
import 'package:neoroo/screens/vitals/vitals.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  PersistentTabController _controller =
      PersistentTabController(initialIndex: 0);
  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      tabs: [
        PersistentTabConfig(
          screen: ScanQrCodeScreen(),
          item: ItemConfig(
            icon: Icon(Icons.home),
            title: AppLocalizations.of(context)!.home,
          ),
        ),
        PersistentTabConfig(
          screen: SkinToSkinTimeScreen(),
          item: ItemConfig(
            icon: FaIcon(FontAwesomeIcons.baby),
            title: AppLocalizations.of(context)!.sts,
          ),
        ),
        PersistentTabConfig(
          screen: VitalsPage(),
          item: ItemConfig(
            icon: FaIcon(FontAwesomeIcons.heartPulse),
            title: AppLocalizations.of(context)!.vitals,
          ),
        ),
        PersistentTabConfig(
          screen: ChatHomePage(),
          item: ItemConfig(
          icon: Icon(Icons.chat),
            title: "Messaging",
          ),
        ),
        PersistentTabConfig(
          screen: MoreOptions(),
          item: ItemConfig(
            icon: Icon(Icons.menu),
            title: AppLocalizations.of(context)!.more,
          ),
        ),
      ],
      avoidBottomPadding: true,
      controller: _controller,
      backgroundColor: Colors.white,
      handleAndroidBackButtonPress: true,
      resizeToAvoidBottomInset: true,
      stateManagement: true,
      screenTransitionAnimation: ScreenTransitionAnimation(
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      navBarBuilder: (config) => Style1BottomNavBar(
        navBarConfig: config,
        navBarDecoration: NavBarDecoration(
          color: Colors.white,
        ),
      ),
    );
  }
}
