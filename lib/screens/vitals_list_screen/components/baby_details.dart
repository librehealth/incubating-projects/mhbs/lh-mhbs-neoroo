import 'package:flutter/material.dart';
import 'package:neoroo/screens/landing_screen/components/main_button.dart';
import 'package:neoroo/utils/methods.dart';

class BabyDetailsCard extends StatelessWidget {
  const BabyDetailsCard({
    super.key, required this.name, required this.birthDate, required this.birthTime, required this.weight, required this.imageURL, required this.onButtonPress,
    this.buttonText = "View Vitals",
    this.buttonWidth= 103,
    this.buttonHeight= 30,
    this.buttonFontSize= 12,
    this.hasQRButton = false,
    this.onQRButtonPress,
  });
  final String name;
  final String birthDate;
  final String birthTime;
  final String weight;
  final String imageURL;
  final VoidCallback onButtonPress;
  final String buttonText;
  final double buttonWidth;
  final double buttonHeight;
  final double buttonFontSize;
  final bool hasQRButton;
  final VoidCallback? onQRButtonPress;
  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight = MediaQuery.of(context).size.height;
    return Padding(
      padding: EdgeInsets.only(left: convertXdWidthToScreen(xdWidth: 30, deviceWidth: MediaQuery.of(context).size.width, xdScreenWidth: 360),
        right: convertXdWidthToScreen(xdWidth: 30, deviceWidth: MediaQuery.of(context).size.width, xdScreenWidth: 360),
        top: convertXdHeightToScreen(xdHeight: 27, deviceHeight: MediaQuery.of(context).size.height, xdScreenHeight: 640),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                'Baby $name',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w500,

                ),
              ),
            ],
          ),
          SizedBox(height: convertXdHeightToScreen(xdHeight: 16, deviceHeight: MediaQuery.of(context).size.height, xdScreenHeight: 640),),
          Row(
            children: [
              Container(
                width: convertXdWidthToScreen(
                    xdWidth: (deviceWidth > deviceHeight && deviceWidth > 600)
                        || deviceWidth > 960 ?45:98,
                    deviceWidth: MediaQuery.of(context).size.width,
                    xdScreenWidth: 360),
                height: convertXdWidthToScreen(
                    xdWidth: (deviceWidth > deviceHeight && deviceWidth > 600)
                        || deviceWidth > 960 ?45:98,
                    deviceWidth: MediaQuery.of(context).size.width,
                    xdScreenWidth: 360),
                decoration: ShapeDecoration(
                  image: DecorationImage(
                    image: NetworkImage(imageURL),
                    fit: BoxFit.cover,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.5*convertXdWidthToScreen(
                        xdWidth: 98,
                        deviceWidth: MediaQuery.of(context).size.width,
                        xdScreenWidth: 360),),
                  ),
                ),
              ),
              SizedBox(width: convertXdWidthToScreen(xdWidth: 30, deviceWidth: MediaQuery.of(context).size.width, xdScreenWidth: 360),),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'Birth Date: ',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        TextSpan(
                          text: birthDate,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 8,),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'Birth Time: ',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        TextSpan(
                          text: birthTime,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 8,),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'Current Weight: ',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        TextSpan(
                          text: weight,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
          Row(
            children: [
              Container(
                width: convertXdWidthToScreen(
                  xdWidth: (deviceWidth > deviceHeight && deviceWidth > 600)
                      || deviceWidth > 960 ?45:98,
                  deviceWidth: MediaQuery.of(context).size.width,
                  xdScreenWidth: 360),
                color: Colors.black,
              ),
              SizedBox(width: convertXdWidthToScreen(xdWidth: 20, deviceWidth: MediaQuery.of(context).size.width, xdScreenWidth: 360),),
              Expanded(
                child: Row(
                  mainAxisAlignment: (deviceWidth > deviceHeight && deviceWidth > 600)
                      || deviceWidth > 960 ?MainAxisAlignment.end:MainAxisAlignment.start,
                  children: [
                    hasQRButton? // make a circular button with a QR code icon using elevated button
                    ElevatedButton(
                      onPressed: onQRButtonPress,
                      child: Icon(Icons.qr_code_2_outlined),
                      style: ElevatedButton.styleFrom(
                        foregroundColor: Colors.white, backgroundColor: Colors.black, shape: CircleBorder(),
                        padding: EdgeInsets.all(10),
                        elevation: 0,
                      ),
                    ): Container(),
                    MainButton(
                      deviceWidth: MediaQuery.of(context).size.width,
                      deviceHeight: MediaQuery.of(context).size.height,
                      rounded: false,
                      title: buttonText, onPressed: onButtonPress,
                      fontWeight: FontWeight.w500,
                      textSize: buttonFontSize,
                      buttonSize: Size(convertXdWidthToScreen(xdWidth: buttonWidth, deviceWidth: MediaQuery.of(context).size.width, xdScreenWidth: 360),
                          convertXdHeightToScreen(xdHeight: buttonHeight, deviceHeight: MediaQuery.of(context).size.height, xdScreenHeight: 640)),),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: convertXdHeightToScreen(xdHeight: 20, deviceHeight: MediaQuery.of(context).size.height, xdScreenHeight: 640),),

          Divider(
            color: Color.fromARGB(255, 0, 0, 0).withOpacity(0.1),
            thickness: 1,
          ),
        ],
      ),
    );
  }
}
