import 'package:flutter/material.dart';
import 'package:neoroo/models/vitals_arguments.dart';
import 'package:neoroo/screens/notifications_screen/notifications_screen.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/screens/vitals_list_screen/components/baby_details.dart';
import 'package:neoroo/routing/routing.dart' as router;
import 'package:neoroo/screens/vitals_screen/vitals_screen.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';
class VitalsListScreen extends StatefulWidget {
  const VitalsListScreen({Key? key}) : super(key: key);

  @override
  State<VitalsListScreen> createState() => _VitalsListScreenState();
}

class _VitalsListScreenState extends State<VitalsListScreen> {
  late TextEditingController searchController;
  final List<Map<String, String>> babyDetails= [
    {"name": "Mila", "birthDate": "20/07/2020","weight":"1500 gm", "birthTime":"9:30 AM", "imageURL":"https://media.istockphoto.com/id/636253690/photo/newborn-baby-girl-sleeping-in-bowl.jpg?s=612x612&w=0&k=20&c=XUvinWRoz4oyXdPEoMcEUgNJMAlxjLBr8fwlSHbX8TU="},
    {"name": "Nia", "birthDate": "20/07/2020","weight":"1500 gm", "birthTime":"9:30 AM", "imageURL":"https://media.istockphoto.com/id/636253690/photo/newborn-baby-girl-sleeping-in-bowl.jpg?s=612x612&w=0&k=20&c=XUvinWRoz4oyXdPEoMcEUgNJMAlxjLBr8fwlSHbX8TU="},
    {"name": "Nia", "birthDate": "20/07/2020", "weight":"1500 gm","birthTime":"9:30 AM", "imageURL":"https://media.istockphoto.com/id/636253690/photo/newborn-baby-girl-sleeping-in-bowl.jpg?s=612x612&w=0&k=20&c=XUvinWRoz4oyXdPEoMcEUgNJMAlxjLBr8fwlSHbX8TU="},
    {"name": "Nia", "birthDate": "20/07/2020", "weight":"1500 gm","birthTime":"9:30 AM", "imageURL":"https://media.istockphoto.com/id/636253690/photo/newborn-baby-girl-sleeping-in-bowl.jpg?s=612x612&w=0&k=20&c=XUvinWRoz4oyXdPEoMcEUgNJMAlxjLBr8fwlSHbX8TU="},
  ];
  @override
  void initState() {
    super.initState();
    searchController = TextEditingController();
  }
  VitalsArguments arguments = VitalsArguments(
    temperatureSeverity: VitalSeverity.high,
    heartRateSeverity: VitalSeverity.low,
    oxygenSaturationSeverity: VitalSeverity.normal,
    respirationSeverity: VitalSeverity.normal,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ScreenHeader(
          isUserIconVisible: false,
          title: "Vitals Tracker",
          actionIconImages: [
            "assets/images/notification.png",
          ],
          actionIconOnPressFunctions: [
                () {
                  pushScreen(context, screen: NotificationsScreen(), settings: RouteSettings(name: router.notificationsPage));

                },
          ],
          hasSearchBar: true,
          searchController: searchController,
          searchHintText: "Type moms’ name or choose birth date",
          onSearchTextChanged: (text) {
            setState(() {
              // search text
            });
          },
      ),
      body: SingleChildScrollView(
        child: Column(
          children: babyDetails.map((e) =>
          BabyDetailsCard(
            name: e["name"]!,
            birthDate: e["birthDate"]!,
            birthTime: e["birthTime"]!,
            weight: e["weight"]!,
            imageURL: e["imageURL"]!,
            hasQRButton: true,
            onQRButtonPress: (){
              // TODO:: Navigate to QR code view screen
            },
            onButtonPress: (){
              pushScreen(context, screen:VitalsScreen(
                temperatureSeverity: arguments.temperatureSeverity,
                heartRateSeverity: arguments.heartRateSeverity,
                oxygenSaturationSeverity: arguments.oxygenSaturationSeverity,
                respirationSeverity: arguments.respirationSeverity,
              ), settings: RouteSettings(name: router.vitalsTypePage, arguments: arguments ));
            },
          )).toList(
          )
        ),
      )
    );
  }
}

