import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:neoroo/screens/heart_rate_screen/heart_rate_screen.dart';
import 'package:neoroo/screens/home_screen/home_page.dart';
import 'package:neoroo/screens/landing_screen/landing_screen.dart';
import 'package:neoroo/screens/more_options_screen/more_options_screen.dart';
import 'package:neoroo/screens/oxygen_rate_screen/oxygen_rate_screen.dart';
import 'package:neoroo/screens/raspiration_screen/raspiration_screen.dart';
import 'package:neoroo/screens/temperature_screen/temperature_screen.dart';
import 'package:neoroo/screens/training_module_screen/training_module_screen.dart';
import 'package:neoroo/screens/training_modules_list/training_modules_list_screen.dart';
import 'package:neoroo/screens/vitals_list_screen/vitals_list_screen.dart';
import 'package:neoroo/screens/vitals_screen/vitals_screen.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';

class MainNavigationScreen extends StatefulWidget {
  const MainNavigationScreen({Key? key}) : super(key: key);

  @override
  State<MainNavigationScreen> createState() => _MainNavigationScreenState();
}

class _MainNavigationScreenState extends State<MainNavigationScreen> {

  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight= MediaQuery.of(context).size.height;
    double iconSize = convertXdWidthToScreen(xdWidth: 24, deviceWidth: deviceWidth, xdScreenWidth: 360);
    double imageSize = convertXdWidthToScreen(xdWidth: 24, deviceWidth: deviceWidth, xdScreenWidth: 360);


    return SafeArea(
      child: Scaffold(
       appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
        // Status bar color
        statusBarColor: primary,
        // Status bar brightness (optional)
        statusBarIconBrightness:  Brightness.light, // For Android (dark icons)
        statusBarBrightness:Brightness.dark, // For iOS (dark icons)
    ),),
        bottomNavigationBar: PersistentTabView(
          navBarHeight: kBottomNavigationBarHeight+10,
          controller: PersistentTabController(initialIndex: 0),
          tabs: [
            PersistentTabConfig(
              screen: HomePage(
                userName: "Everlyne",
                userPictureURL: "assets/images/Everlyne_profile.png",
                alerts: [
                  {"title": "\"Baby Nia's respiratory rate is 72 breaths per minute (too high). Additional assessment required; postnatal ward, bed 6.\"",
                    "severity": VitalSeverityType.high,
                    "type": VitalType.bloodOxygen,
                    "todo": "Check baby's blood oxygen again in 30 minutes"
                  },
                  {"title": "\"Baby Nia's respiratory rate is 72 breaths per minute (too high). Additional assessment required; postnatal ward, bed 6.\"",
                    "severity": VitalSeverityType.low,
                    "type": VitalType.heartRate,
                    "todo": "Check baby's heart rate again in 30 minutes"
                  },
                  {"title": "\"Baby Nia's respiratory rate is 72 breaths per minute (too high). Additional assessment required; postnatal ward, bed 6.\"",
                    "severity": VitalSeverityType.low,
                    "type": VitalType.respiratoryRate,
                    "todo": "Check baby's respiratory rate again in 30 minutes"
                  },
                  {"title": "\"Baby Nia's respiratory rate is 72 breaths per minute (too high). Additional assessment required; postnatal ward, bed 6.\"",
                    "severity": VitalSeverityType.high,
                    "type": VitalType.temperature,
                    "todo": "Take temperature again in 30 minutes"
                  },
                ],
              ),
              item: ItemConfig(
                icon: Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Image.asset("assets/images/home_active.png",height: imageSize,),
                ),
                inactiveIcon: Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Image.asset("assets/images/home_inactive.png", height: imageSize,),
                ),
                title: ("Home"),
                iconSize: imageSize,
              ),
            ),
            PersistentTabConfig(
              screen: VitalsListScreen(),
              item: ItemConfig(
                icon: Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Image.asset("assets/images/vitals-active.png",height: imageSize,),
                ),
                inactiveIcon: Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Image.asset("assets/images/vitals.png", height: imageSize,),
                ),
                title: ("Vitals"),
              ),
            ),
            PersistentTabConfig(
              screen: Container(),
              item: ItemConfig(
                  icon: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Image.asset("assets/images/skin-to-skin-active.png", height: imageSize, ),
                  ),
                  inactiveIcon: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Image.asset("assets/images/skin-to-skin.png", height: imageSize,),
                  ),
                  title: ("Skin-Skin"),
                  iconSize: imageSize
              ),
            ),
            PersistentTabConfig(
              screen: Container(),
              item: ItemConfig(
                icon: Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Image.asset("assets/images/high_alerts_Active.png",height: imageSize,),
                ),
                inactiveIcon: Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Image.asset("assets/images/high_alerts_inactive.png", height: imageSize,),
                ),
                title: ("High Alerts"),
                iconSize: imageSize,
              ),
            ),
            PersistentTabConfig(
              screen: MoreOptionsScreen(),
              item: ItemConfig(
                icon: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Image.asset("assets/images/more-options-active.png", height: imageSize,),
                ),
                inactiveIcon: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Image.asset("assets/images/more-options.png", height: imageSize,),
                ),
                title: ("More"),
              ),
            )
          ],
          avoidBottomPadding: true,
          backgroundColor: Colors.white,
          handleAndroidBackButtonPress: true,
          resizeToAvoidBottomInset: true,
          stateManagement: true,
          popAllScreensOnTapOfSelectedTab: true,
          popActionScreens: PopActionScreensType.all,
          screenTransitionAnimation: const ScreenTransitionAnimation(
            curve: Curves.ease,
            duration: Duration(milliseconds: 200),
          ),

          navBarBuilder: (config) => Style4BottomNavBar(
            navBarConfig: config,
            navBarDecoration: NavBarDecoration(
              color: Colors.white,
              border: Border(
                top: BorderSide(
                  color: Color(0xffE1E1E1),
                  width: 0.51,
                ),
              )
            ),
          ),
        ),
      ),
    );
  }
}
