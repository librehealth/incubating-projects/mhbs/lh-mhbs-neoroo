import 'package:flutter/material.dart';
import 'package:neoroo/screens/authentication_screens/login_screen.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:neoroo/utils/vertical_space.dart';

import '../../utils/enums.dart';
import 'components/choose_user_type_button.dart';
import 'package:neoroo/routing/routing.dart' as router;
class ChooseUserTypeScreen extends StatelessWidget {
  const ChooseUserTypeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight= MediaQuery.of(context).size.height;
    return Scaffold(
      body: Column(
        mainAxisAlignment: (deviceWidth > deviceHeight && deviceWidth > 600)
            || deviceWidth > 960 ? MainAxisAlignment.spaceEvenly: MainAxisAlignment.start,
        children: [
          VerticalSpace(height: (deviceWidth > deviceHeight && deviceWidth > 600)
              || deviceWidth > 960 ? 0:
          convertXdHeightToScreen(xdHeight: 96, deviceHeight: deviceHeight)),
          Image.asset("assets/images/logo-title.jpg",
          width:
          (deviceWidth > deviceHeight && deviceWidth > 600)
              || deviceWidth > 960 ? null:
          convertXdWidthToScreen(xdWidth: 169, deviceWidth: deviceWidth),
            height: (deviceWidth > deviceHeight && deviceWidth > 600)
                || deviceWidth > 960 ? convertXdHeightToScreen(xdHeight: 96, deviceHeight: deviceHeight): null,

          ),
          VerticalSpace(height:
          (deviceWidth > deviceHeight && deviceWidth > 600)
              || deviceWidth > 960 ? 0:
          convertXdHeightToScreen(xdHeight: 78, deviceHeight: deviceHeight)),
          ChooseUserTypeButton(
            userType: UserType.familyMember,
            onPressed: () {
              // TODO (Mehul): set the global user role to family member
              Navigator.pushNamed(context, router.parentLoginPage);

            },
          ),
          VerticalSpace(height:
          (deviceWidth > deviceHeight && deviceWidth > 600)
              || deviceWidth > 960 ? 0:
          convertXdHeightToScreen(xdHeight: 121, deviceHeight: deviceHeight)),

          ChooseUserTypeButton(
            userType: UserType.careProvider,
            onPressed: () {
              // TODO (Mehul): set the global user role to care provider
                Navigator.pushNamed(context, router.nurseLoginPage);
            },
          ),
        ],
      ),
    );
  }
}
