import 'package:flutter/material.dart';
import 'package:neoroo/utils/constants.dart';

class OptionTile extends StatelessWidget {
  const OptionTile({
    super.key,
    required this.onTap,
    required this.title,
  });
  final VoidCallback onTap;
  final String title;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.all(16),
      title: Text(
        title,
        style: TextStyle(
          color: Colors.black,
          fontSize: 18,
          fontFamily: 'Roboto',
          fontWeight: FontWeight.w500,
        ),
      ),
      onTap: onTap,
      trailing: Icon(Icons.arrow_forward_ios_rounded, size: 16,
        color: primary,),
      tileColor: Color(0xFFFBFBFB),
    );
  }
}
