import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';  // Add this
import 'package:neoroo/bloc/more_options/more_options_bloc.dart';  // Add this
import 'package:neoroo/bloc/more_options/more_options_events.dart';  // Add this
import 'package:neoroo/screens/babies_list_screen/babies_list_screen.dart';
import 'package:neoroo/screens/more_options_screen/components/contact_dialog.dart';
import 'package:neoroo/screens/more_options_screen/components/option_tile.dart';
import 'package:neoroo/screens/notifications_screen/notifications_screen.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/screens/training_modules_list/training_modules_list_screen.dart';
import 'package:neoroo/screens/landing_screen/landing_screen.dart';  // Add this
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';
import 'package:neoroo/routing/routing.dart' as router;  
import 'package:neoroo/bloc/more_options/more_options_states.dart';  // Add this

class MoreOptionsScreen extends StatelessWidget {
  const MoreOptionsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double deviceHeight = MediaQuery.of(context).size.height;
    double deviceWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: ScreenHeader(
        title: "More Options",
        isUserIconVisible: false,
        actionIconImages: [
          "assets/images/notification.png"
        ],
        actionIconOnPressFunctions: [
          () {
            pushScreen(context,
                screen: NotificationsScreen(),
                settings: RouteSettings(name: router.notificationsPage));
          }
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: convertXdWidthToScreen(
                  xdWidth: 8, deviceWidth: deviceWidth, xdScreenWidth: 360),
              vertical: convertXdHeightToScreen(
                  xdHeight: 16,
                  deviceHeight: deviceHeight,
                  xdScreenHeight: 640)),
          child: Column(
            children: [
              OptionTile(
                title: "Edit Baby Details",
                onTap: () {
                  pushScreen(context,
                      screen: BabiesListScreen(),
                      settings:
                          RouteSettings(name: router.chooseBabyToEditorAdd));
                },
              ),
              SizedBox(
                height: convertXdHeightToScreen(
                    xdHeight: 4,
                    deviceHeight: deviceHeight,
                    xdScreenHeight: 640),
              ),
              OptionTile(
                title: "Training Modules",
                onTap: () {
                  pushScreen(context,
                      screen: TrainingModulesList(),
                      settings:
                          RouteSettings(name: router.trainingModulesListPage));
                },
              ),
              SizedBox(
                height: convertXdHeightToScreen(
                    xdHeight: 4,
                    deviceHeight: deviceHeight,
                    xdScreenHeight: 640),
              ),
              OptionTile(
                title: "Starred Resources",
                onTap: () {},
              ),
              SizedBox(
                height: convertXdHeightToScreen(
                    xdHeight: 4,
                    deviceHeight: deviceHeight,
                    xdScreenHeight: 640),
              ),
              OptionTile(
                title: "Help & Support",
                onTap: () {
                  showAdaptiveDialog(
                    context: context,
                    builder: (context) {
                      return ContactsDialog(
                          email: "donya.esawi@gmail.com",
                          phone: "+20 - 1141 - 9921 - 110",
                          name: "Donia Esawy");
                    },
                  );
                },
              ),
              SizedBox(
                height: convertXdHeightToScreen(
                  xdHeight: 4,
                  deviceHeight: deviceHeight,
                  xdScreenHeight: 640,
                ),
              ),
              OptionTile(
                title: "Sign Out",
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text('Sign Out'),
                        content: const Text('Are you sure you want to sign out?'),
                        actions: [
                          TextButton(
                            onPressed: () => Navigator.pop(context),
                            child: const Text('Cancel'),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context); // Close dialog
                              Navigator.of(context, rootNavigator: true)
                               .pushNamedAndRemoveUntil(
                                  router.landingPage,
                                  (route) => false,
                              );
                            },
                            style: TextButton.styleFrom(
                              foregroundColor: Colors.red,
                            ),
                            child: const Text('Sign Out'),
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
              SizedBox(
                height: convertXdHeightToScreen(
                    xdHeight: 4,
                    deviceHeight: deviceHeight,
                    xdScreenHeight: 640),
              ),
            ],
          ),
        ),
      ),
    );
  }
}