
// Define Routes
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:neoroo/models/home_page_arguments.dart';
import 'package:neoroo/models/vitals_arguments.dart';
import 'package:neoroo/screens/add_edit_baby_details/add_edit_baby_details.dart';
import 'package:neoroo/screens/authentication_screens/choose_organization.dart';
import 'package:neoroo/screens/authentication_screens/login_screen.dart';
import 'package:neoroo/screens/babies_list_screen/babies_list_screen.dart';
import 'package:neoroo/screens/choose_user_type/choose_user_type_screen.dart';
import 'package:neoroo/screens/heart_rate_screen/heart_rate_screen.dart';
import 'package:neoroo/screens/home_screen/home_page.dart';
import 'package:neoroo/screens/landing_screen/landing_screen.dart';
import 'package:neoroo/screens/more_options_screen/more_options_screen.dart';
import 'package:neoroo/screens/navigator_screen/main_navigator.dart';
import 'package:neoroo/screens/notifications_screen/notifications_screen.dart';
import 'package:neoroo/screens/oxygen_rate_screen/oxygen_rate_screen.dart';
import 'package:neoroo/screens/raspiration_screen/raspiration_screen.dart';
import 'package:neoroo/screens/temperature_screen/temperature_screen.dart';
import 'package:neoroo/screens/training_module_screen/training_module_screen.dart';
import 'package:neoroo/screens/training_modules_list/training_modules_list_screen.dart';
import 'package:neoroo/screens/vitals_list_screen/vitals_list_screen.dart';
import 'package:neoroo/screens/vitals_screen/vitals_screen.dart';
import 'package:neoroo/utils/enums.dart';

// Route Names
const String landingPage  = '/';
const String chooseUserTypePage = '/chooseUserType';
const String nurseLoginPage    = '/nurseLogin';
const String parentLoginPage    = '/parentLogin';
const String chooseOrganisationPage = '/chooseOrganisation';
const String mainNavScreen     = '/mainNav';
//const String STSPage      = '/STS';
//const String chooseBabyToViewVitalsPage = '/chooseBabyToViewVitals';
const String vitalsTypePage = '/vitalsType';
const String temperaturePage = '/temperature';
const String respirationPage = '/respiration';
const String heartRatePage = '/heartRate';
const String spo2Page = '/spo2';
//const String optionsPage = '/options';
const String chooseBabyToEditorAdd = '/chooseBabyToEdit';
const String addOrEditBabyPage = '/editBaby';
const String trainingModulesListPage = '/trainingModulesList';
const String trainingModulePage = '/trainingModule';
const String notificationsPage = '/notifications';
const String homePage = '/home';

// Control our page route flow
Route<dynamic> controller(RouteSettings settings) {
  switch (settings.name) {
    case landingPage:
      return MaterialPageRoute(builder: (context) => LandingScreen());
    case chooseUserTypePage:
      return MaterialPageRoute(builder: (context) => ChooseUserTypeScreen());
    case nurseLoginPage:
      return MaterialPageRoute(builder: (context) => LoginScreen(userType: UserType.careProvider));
    case parentLoginPage:
      return MaterialPageRoute(builder: (context) => LoginScreen(userType: UserType.familyMember));
    case chooseOrganisationPage:
      return MaterialPageRoute(builder: (context) => ChooseOrgScreen());
    case mainNavScreen:
      return MaterialPageRoute(builder: (context) => MainNavigationScreen());
    case homePage:
      if (settings.arguments == null) {
      throw('make sure to pass the correct arguments to the home page');
      }
      final args = settings.arguments! as HomePageArguments;
      return MaterialPageRoute(builder: (context) => HomePage(
          userName: args.userName,
          userPictureURL: args.userPictureURL,
          alerts: args.alerts // This should now match List<Map<String, dynamic>>
        ));
    // case STSPage:
    //   return MaterialPageRoute(builder: (context) => Container()); // TODO:: add STS page that Mehul created
    // case chooseBabyToViewVitalsPage:
    //   return MaterialPageRoute(builder: (context) => VitalsListScreen());
    case vitalsTypePage:
      if (settings.arguments == null) {
        throw('make sure to pass the correct arguments to the home page');
      }

      return MaterialPageRoute(builder: (context) => VitalsScreen(
        temperatureSeverity: (settings.arguments! as VitalsArguments).temperatureSeverity,
        respirationSeverity: (settings.arguments! as VitalsArguments).respirationSeverity,
        heartRateSeverity: (settings.arguments! as VitalsArguments).heartRateSeverity,
        oxygenSaturationSeverity: (settings.arguments! as VitalsArguments).oxygenSaturationSeverity,
      ));
    case temperaturePage:
      return MaterialPageRoute(builder: (context) => TemperatureScreen());
    case respirationPage:
      return MaterialPageRoute(builder: (context) => RespirationRateScreen());
    case heartRatePage:
      return MaterialPageRoute(builder: (context) => HeartRateScreen());
    case spo2Page:
      return MaterialPageRoute(builder: (context) => SpO2RateScreen());
    // case optionsPage:
    //   return MaterialPageRoute(builder: (context) => MoreOptionsScreen());
    case chooseBabyToEditorAdd:
      return MaterialPageRoute(builder: (context) => BabiesListScreen());
    case addOrEditBabyPage:
      return MaterialPageRoute(builder: (context) => AddEditBabyDetails());
    case trainingModulesListPage:
      return MaterialPageRoute(builder: (context) => TrainingModulesList());
    case trainingModulePage:
      return MaterialPageRoute(builder: (context) => TrainingModuleScreen());
    case notificationsPage:
      return MaterialPageRoute(builder: (context) => NotificationsScreen());
    default:
      log(settings.name!);
      throw(' :: This route name does not exit');
  }
}