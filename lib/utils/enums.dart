enum UserType { familyMember, careProvider }
enum InputType {email, password, username, number, datetime, text, serverURL}
enum ToolbarIconsTheme {light, dark}
enum VitalSeverityType {low, high}
enum VitalType {bloodOxygen, temperature, heartRate, respiratoryRate}
enum Unit {Celsius, Fahrenheit}
enum VitalSeverity {low, normal, high}