// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';
import 'package:neoroo/exceptions/custom_exception.dart';
import 'package:neoroo/models/infant_model.dart';
import 'package:neoroo/models/infant_mother.dart';

abstract class AddBabyStates extends Equatable {}

class InitialAddBabyState extends AddBabyStates {
  @override
  List<Object?> get props => [];
}

class LoadingAddBaby extends AddBabyStates {
  @override
  List<Object?> get props => [];
}

class AddBabySuccess extends AddBabyStates {
  @override
  List<Object?> get props => [];
}

class AddBabyError extends AddBabyStates {
  final CustomException exception;
  AddBabyError({required this.exception});

  @override
  List<Object?> get props => [];
}

class SearchMotherState extends AddBabyStates {
  final List<Mother> motherList;

  SearchMotherState(this.motherList);

  @override
  List<Object?> get props => [];
}

class SearchMotherInitialState extends AddBabyStates {
  @override
  List<Object?> get props => [];
}

class AddBabyEmptyField extends AddBabyStates {
  @override
  List<Object?> get props => [];
}

class FetchInfantFromECEBInitialState extends AddBabyStates {
  @override
  List<Object?> get props => [];
}

class FetchBabyFromECEBSucess extends AddBabyStates {
  final List<Infant> ecebInfantsOnServer;
  FetchBabyFromECEBSucess({
    required this.ecebInfantsOnServer,
  });

  @override
  List<Object?> get props => [];
}

class EcebInfantSelectedState extends AddBabyStates {
  final Infant infant;
  EcebInfantSelectedState({
    required this.infant,
  });

  @override
  List<Object?> get props => [];
}
